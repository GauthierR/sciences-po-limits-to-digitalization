# Collective project [3/3]


#### Activity: Starting collective project (2h)
	After being given instructions, each group works on its assignment


## Session goal

Make an assessment of climate and environmental risks (present and future), and transition policies of the city assigned to your group by the teacher.

## Role

You are civil servants being asked to deliver a recommendation for the city council and the mayor. Your analysis will help the city refuse, transform or accept the deployment of a given technology. Your analysis will help determine whether the city should invest in or deploy this technology.

You need to answer the following question: **is this digital technology helping to achieve the city's transition objectives at scale and in the long term, or is it more of a short-term gamble with an uncertain chance of success or low-yield results?** You need to identify the conditions that would enable this technology to help achieve the transition objectives, but also the conditions that would prevent it from doing so. You need to identify whether these conditions are achievable and maintainable in your city, based on the analyses of the previous sessions.


## Instructions for the collective project

* Make groups of 4 people
* Each group will present their final analysis and recommendation in 20 minutes
* Each group will also provide a one-page written summary of their recommendation, plus the assessments from session 7 and 10


## Deliverable

* Assessment from session 7
* Assessment from session 10
* One-page sumamry of your recommendation
* Presentation file (pptx, pdf, etc.)


