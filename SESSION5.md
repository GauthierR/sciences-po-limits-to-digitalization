# Session 5 - The environmental footprint of the digital sector

> This session parlty uses the research paper from Gauthier Roussilhe, Anne-Laure Ligozat and Sophie Quinton, titled: A long road ahead: a review of the state of knowledge of the environmental effects of digitization (2023) ([Link](https://www.sciencedirect.com/science/article/pii/S187734352300043X?dgcid=author)).

## A key difference

For the purpose of environmental analysis of the digital sector, it is customary to divide it into three technical parts: data centers, networks and user equipment. In addition, there is a services perimeter: with a few exceptions, any digital service needs data centers, telecommunication networks and IT equipment to function. Separately, we look at other sectors (transport, energy, industry, etc.) to estimate the indirect effects of digitization (rebound effect, substitution, etc. leading to avoided or added impacts) without really imposing an official method. **This session will only look at the environmental footprint of the digital sector**.


<!--To summarize: 
* The ICT sector's footprint exposes direct effects
	* Greenhouse gases emission
	* Energy consumption
	* Material consumption
	* Water consumption
	* ...
* Digitalization triggers indirect effects that will modify the footprint of other sectors
	* Efficiency
	* Substitution
	* Rebound effect
	* ...

The digital sector generally includes the ICT sector and the Entertainment & Media sector (E&M). The latter then defines the sector of production and publication of complementary content (publishing activities, film & television, broadcasting and programming activities, other information services) in parallel to the ICT sector.-->

<br>
<img src="img/S5-01.jpg" alt="The digital sector and digitalization" style="border: 1px solid gray;"><br>


## What is an environmental footprint?

In environmental studies, the digital sector is divided into three areas: data centres, telecommunications networks and user equipment. This division is now the consensus of the scientific community and also meets the modelling objectives of international standards for studying the life cycle of a system, a service or a product. These standards are ISO 14040 and 140441 and specifically the ITU L.1410 standard (ETSI 203 199) developed in part by the industry.

Describing all the environmental impacts related to the production of digital infrastructure (manufacture of servers, antennas, equipment, etc.), its use and its end of life, is a complex exercise that varies greatly depending on the equipment, the production methods and the territories concerned. This description of impacts can only be accurate if it focuses on equipment production and use at an explicitly defined scale. For example, what are the environmental impacts associated with the production, use and end-of-life of a smartphone?

<br>
<iframe width="100%" height="480px" frameborder="0" src="https://open.sourcemap.com:443/maps/embed/61a98acef1ddeb086156a529"></iframe><br>
[See the supply chain of Fairphone 4](https://open.sourcemap.com/maps/embed/61a98acef1ddeb086156a529)

The digital infrastructure has multiple impacts on ecosystems: use of fresh water, extraction of minerals in fragile ecosystems, soil pollution, landfill pollution from equipment, greenhouse gas emissions, etc. One indicator in particular is attracting attention today because it corresponds to the transition objectives set by national and international institutions: greenhouse gas emissions. These gases are generally emitted in the production of IT equipment (fuel oil for extraction and transport, energy to run industrial production lines, foundries, etc.) and in the production of the energy needed to run this equipment (electricity production). Greenhouse gas emissions are therefore directly related to the energy consumption of the digital infrastructure, i.e. the manufacture and use of equipment. However, despite the focus on greenhouse gas emissions and thus energy consumption, other major impacts of digital infrastructure should not be overlooked: consumption of metals and other associated resources, water consumption, ecosystem degradation and pollution. In order to have the clearest possible picture of the environmental footprint of digital technology, it is necessary to use all these criteria and to study them with a globally shared methodology. That's the purpose of a life cycle assessment (LCA).

<br>
<img src="img/S5-02.png" alt="Environmental footprint of a digital service - Simplified LCA" style="border: 1px solid gray;"><br>
<i>Environmental footprint of a simplified LCA for a digital service</i><br>

Today, we have relatively little data on the entire life cycle of digital products and infrastructure, especially on manufacturing and end-of-life. In fact, the environmental footprint is limited to four environmental factors, whereas the European Union recommends 20 in its environmental analysis standards.


#### Activity: Database exploration (15 minutes)

	Go to Boavizta's database and look for the environmental impacts 
	of digital equipment during the manufacturing and use phases.

[Link to database](https://dataviz.boavizta.org/) 

<!-- ### Energy consumption
The first question that needs to be asked is the type of equipment and machinery used in each phase of the life cycle and the type of energy that powers them. A backhoe or a truck on a mine site both need fuel to operate, the ore crushing operation requires a very large amount of energy supplied by oil-fired generators, the ship that will transport the ores to the refinery will also consume oil. The plant that smelts the ore into ingots will be dependent on a power station fuelled by coal, gas, fuel oil or other primary energy sources. An assembly line will also use electricity generated from different energy sources depending on the country. But energy consumption in manufacturing is not just about extracting minerals, for example, there is the tapping of rubber trees to produce the rubber that coats much of the cable, and the production of plastic components by the petrochemical industry, among many other energy-intensive steps in manufacturing this infrastructure. Once digital equipment has been manufactured, delivered, sold and installed, it needs to be powered to operate. We use a well-known energy carrier to power them: electricity. We plug the vast majority of our digital devices into a socket and power them via an electric current.

### GHG emissions
There are several types of greenhouse gases: water vapour (H 2O ), carbon dioxide (CO2), methane (CH4), nitrous oxide (N2O), perfluorocarbons (CF4), hydrofluorocarbons (HFCs), sulphur hexafluoride (SF6). Each of these gases captures solar radiation to a greater or lesser extent and therefore each has a relative warming power. This warming power also depends on the residence time of these gases in the atmosphere. To calculate the warming power of these gases, the carbon equivalence is used, i.e. their warming power is compared to that of carbon dioxide over a period of 100 years.

In absolute terms, the digital sector is not the one that emits the most greenhouse gases, with energy production, transport and agriculture far ahead. However, digital is one of the sectors with the highest growth rate of these emissions. The more this sector increases its consumption of energy and materials by producing new equipment and increasing electricity production, the more these emissions will increase.


### Material footprint

*See previous session*

### Water footprint

Digital systems use vast amounts of water, whether for mineral extraction, cooling of facilities, or the generation of electricity to power the entire infrastructure, from the data centre to user equipment. A distinction could be made between two types of water footprint, net water consumption, i.e. water consumed directly by the infrastructure, and the use of water flows, i.e. water withdrawn and then returned, such as for cooling a data centre.

Today the largest part of the digital water footprint is in mining operations. Water is needed in almost every stage of metal production. The crushing of the mined ore to obtain a thicker or thinner grain, as well as the concentration phase of the ore, account for 70% of the water consumption of the mining industry. After their manufacture, the use of the user equipment requires the production of electricity, which comes from different primary energy sources. For example, in France it is estimated that 4 litres of fresh water are needed to produce one kWh, often linked to the evaporation of water in nuclear power plants. In addition to direct consumption, the digital infrastructure takes water, for example, to cool data centres, before returning it to a slightly higher temperature. The amount of water flow available each year depends largely on the geological, geographical and climatic conditions of each territory. Thus a water flow in a given territory is not scalable, if the water flow is directed to a new use it is not going anywhere else. Water abstraction from digital infrastructure therefore competes with other sectors, such as agriculture for example. Excess withdrawals can be compensated for by groundwater reserves for a time, if these reserves are allowed to regenerate. 

<br>
![End of life](img/S5-05.png)<br>
<i>Explanations about water footprint</i><br> -->

### Focus on the End of life (EoL)
With the knowledge and data we currently have on the digital infrastructure, the focus is more on the manufacture and use but rarely on the end-of-life of digital equipment. Furthermore, environmental impacts related to greenhouse gases, energy, water and metal consumption are better known than those related to chemicals and more simply to the degradation of ecosystems. Although relatively little data is available, it is necessary to address these other environmental and health impacts to fully appreciate the extent of what remains to be explored regarding the digital infrastructure footprint.

The end of life of an item of equipment generally implies that it is transformed into "waste", i.e. that it is no longer usable or recoverable according to the standards of the equipment manufacturer and of the country producing the "waste." Digital equipment falls into the category of waste electrical and electronic equipment, commonly known as WEEE, This category includes heat exchange equipment (fridge, freezer, air conditioner, screens (TV, laptops, tablets), lamps, large equipment (washing machines, tumble dryers, dishwashers, etc.), small equipment (hoovers, microwave ovens, toasters, etc.), and small IT and telecommunication equipment (mobile phones, GPS, calculators, printers, telephones, routers, smartphones). The waste from digital equipment is therefore in the form of screens and small IT and telecommunications equipment.

Of the 53 Mt of WEEE generated worldwide in 2019, 6.7 Mt came from monitors, tablets, laptops and televisions, and 4.7 Mt came from small IT and telecommunications equipment. Between 2014 and 2019, the generation of waste in these two categories stabilised: the annual generation of waste from screens, tablets, computers and televisions decreased by 1% compared to 2014, while that of small IT equipment increased by 2% for the same period. However, these values do not evolve in the same way between the different regions of the world. Europe produces the highest weight of e-waste per capita, far ahead of Africa and Asia, although the latter produces a large volume of waste, notably due to its larger population. However, Europe is the region with the highest e-waste collection rate thanks to relatively effective national and European policies. Globally, out of 53.6 Mt of e-waste, 44.3 Mt is undocumented and usually ends up buried, traded or recycled outside existing environmental standards (in European countries, 0.6 Mt of e-waste ends up directly in the garbage). However, the value in recoverable raw materials is very high, it is estimated that the metals contained in this waste (aluminium, copper, iron, etc.) are worth 57 billion US dollars (for 53.6 Mt).

Given the phenomenal flow of E-waste being produced and untracked each year, significant pollution is occurring in the countries where this waste is stored and buried. It is estimated that 50 tonnes of mercury escape from landfilled e-waste every year, as well as 71 kt of flame retardant plastics (FRP), which are extremely toxic to the living environment and to the people exposed to them. Informal landfills and recycling sites therefore present significant health and environmental risks. Toxic substances leaking from the oxidation of electronic components pollute soils and waterways, affecting animals, plantations and fish that will be consumed by the surrounding communities. Informal recyclers risk breathing toxic fumes from burning wires and circuit boards. These workers unintentionally put themselves at greater risk of injury as well as genetic damage, blood glucose imbalances, effects on liver function, and fertility disorders.

<br>
![End of life](img/S5-04.png)<br>
<i>Locations of informal e-waste dismantling and recycling sites reported in research literature</i><br>

## Global estimates

So far, most global assessments of the direct environmental effects of ICT have focused on electricity consumption and greenhouse gas (GHG) emissions – also called carbon footprint – leaving out other major effects such as material or water footprint, but also pollutions. The most prominent publications are: Andrae & Edler (2015, 2020), Malmodin & Lunden (2018), Belkhir & Elmeligi (2018) and Freitag et al. (2021). Carbon footprint estimates and projections are computed using a mix of top-down views based on global numbers (worldwide data traffic, hardware shipments, etc.) and bottom-up estimates obtained mostly using life cycle assessments (LCAs). By unifying the scopes considered in the main global estimates, Freitag et al. argued that GHG emissions from the ICT sector ranged from 2.1 to 3.9% of global emissions in 2020 (1.2 to 2.2 GtCO2e). This wide range is explained by different assumptions about the evolution of energy efficiency and ICT decarbonization, but also by the uncertainty surrounding used data sets due to the obfuscation of large parts of the ICT sector’s production and supply chains.

<br>
<img src="img/S5-03.png" alt="Summary of global estimates in 2015(a) and 2020 (b) - Credits: Freitag et al., 2021" style="border: 1px solid gray;"><br>
<i>Summary of global estimates in 2015(a) and 2020 (b) - Credits: Freitag et al., 2021</i><br>

> There is a lack of agreement about which technologies ought to be included in calculations of ICT’s GHG emissions—particularly TV. All studies include data centers, net- works, and user devices as the three main components of ICT, but there are pronounced differences of opinion regarding the proportional impact of each. A comparison of the different pro- portions in 2020 estimates (excluding TV) is provided below (Freitag et al. 2021)

<br>
<img src="img/S5-10.png" alt="Proportional breakdown of ICT’s carbon footprint, excluding TV - Credits: Freitag et al., 2021" style="border: 1px solid gray;"><br>
<i>Proportional breakdown of ICT’s carbon footprint, excluding TV (A) Andrae and Edler (2015): 2020 best case (total of 623 MtCO2e), (B) Belkhir and Elmeligi (2018): 2020 average (total of 1,207 MtCO2e), (C). Malmodin (2020): 2020 estimate (total of 690 MtCO2e) - Credits: Freitag et al., 2021</i><br>


<!--## Regional estimates

There are many ways to estimate the environmental footprint of the digital sector in a given economy or country. There are standard econometric approaches (input/output), specific models using top-down or bottom-up methods. Similarly, the perimeters used may differ from one study to another. Thus, it is very difficult to compare national studies, as presented here with Sweden (specific model), China (input/output model), and France (LCA). This problem of adjustment between the economic classification and the digital sector is well known and is not unique to the environmental sciences (it is also a central problem in economics, for example). In a 2010 research article Erdmann and Hilty recall it as follows:

> This particular set of characteristics of ICT is a challenge to deal with in macroeconomic studies on ICT effects on GHG emissions. There are two basic ways to quantify these effects: (1) incorporate ICTs in general macroeconomic models explicitly or (2) build specific macroeconomic ICT impact assessment models. (Erdmann & Hilty, “Scenario Analysis,” pp. 826-843, 2010)

### In Sweden (1990-2015)

> The energy and carbon footprints of the ICT and E&M sectors is decreasing in Sweden from a top at around 2010. The total carbon footprint is about 1.9% (1.2% ICT, 0.7% E&M) of Sweden’s total carbon footprint using a consumption perspective (production abroad included) and the decrease from 2010 is around 10%. Historically the footprints have been increasing since real measurements begun first at around 1990 but now it seems that a 20 years old trend has been broken and as Sweden is world leading in many ICT and E&M categories this is an important observation. (Malmodin & Lunden, The energy and carbon footprint of the ICT and E&M sector in Sweden 1990-2015 and beyond, p. 217)

<br>
![Environmental footprint - Simplified LCA](img/S5-09.png)<br>
<i>Energy & Carbon footprints of ICT in Sweden</i><br>

### In China (2002-2012)

> The embodied carbon emissions of ICT sector increased by 173 Mt CO2 from 2002 to 2007, largely driven by the significant growth of ICT export volume (+243 Mt). Improvements in emission intensity offset nearly half of the increase (-126 Mt), while emissions reduction driven by changes in production structure was relatively low (-20 Mt). In 2007−2012, the carbon emissions embodied in ICT sector edged up by 39 Mt CO2, due to the much smaller increasing effects of ICT export (+140 Mt). The slowing effects of ICT export could be explained by the loss of partial competitiveness in electronic products for China during the period (Liu et al., 2018b). Meanwhile, the accelerated improvements in the production structure counterbalanced the increase (-116 Mt); this was accompanied by the weakened reduction effect from improvements in emission intensity (-56 Mt). (Zhou et al., How information and communication technology drives carbon emissions: A sector-level analysis for China, p. 20)

<br>
![China](img/S5-08.png)<br>
<i>Changing trends of carbon emissions embodied in ICT subsectors over 2002−2012. Note: Pie charts show the proportion of ICT subsectors with respect to total embodied emissions. The blue lines mark the respective changes of carbon emissions embodied in the ICT clusters of manufacturing and services.</i><br> -->

### In UE (2020)

>Total electricity consumption for digital services in Europe is 283 TWh out of a total of 3,054 TWh6, which means that electricity consumption for digital services during the use phase accounts for 9.3% of European electricity consumption. Total GHG emissions for digital services in Europe are 185 Mt CO2 eq. out of a total of 4,378 Mt CO2 eq.7, which means that GHG emissions from digital services account for 4.2% of European GHG emissions. (GreenIT, Digital Technologies in Europe: an environmental life cycle approach, pp. 8-9)

<br>
<img src="img/S5-06.png" alt="Overall impacts of ICT services in UE - Credits: Bordage et al. 2021" style="border: 1px solid gray;"><br>
<i>Overall impacts and breakdown per tier of ICT services in UE - Credits: Bordage et al. 2021</i><br>

### In France

> Electricity consumption for digital equipment and infrastructures in France is 48.7 TWh, which can be compared with the total of 474.4 TWh, meaning that digital equipment and infrastructures are responsible for 10% of French electricity consumption.<br>
Greenhouse gas emissions from digital equipment and infrastructures in France are estimated at 16.9 Mt CO2 eq, which can be compared with the total 663 MT CO2 eq, meaning that digital equipment and infrastructures are responsible for 2.5% of France's carbon footprint (consumption mix - carbon footprint approach) (Etienne Lees Perasso et al. 2022)

<br>
<img src="img/S5-11.png" alt="Breakdown of the impact of digital equipment and infrastructure per tier - Credits: Lees Perasso et al. 2022" style="border: 1px solid gray;"><br>
<i>Breakdown of the impact of digital equipment and infrastructure per tier - Credits: Lees Perasso et al. 2022</i><br>

### Data centers, networks and devices

> Recent work by Masanet et al. estimates worldwide electricity consumption of data centers at 205 TWh in 2018, a modest 6% increase compared with 2010, despite a 10-fold increase in data traffic. In contrast, based on data sets from the European and German data center industry, Hintemann and Hinterholzer estimated worldwide energy consumption of data centers at around 400 TWh in 2020. The difference between these two figures can be explained by differences in the estimated number of hyperscale data centers (which are particularly efficient), and “differences in scope (e.g. including or excluding cryptomining), methodologies, and assumptions” according to the International Energy Agency (IEA), which recently estimated that data center electricity consumption in 2021 was between 220 and 320 TWh (without including cryptocurrencies) (Roussilhe et al., 2023).

<br>
<img src="img/S5-12.png" alt="Global trends in digital and energy indicators, 2015-2022 - Credits: IEA, 2023" style="border: 1px solid gray;"><br>
<i>Global trends in digital and energy indicators, 2015-2022 - Credits: IEA, 2023</i><br>

> Like for data centers, studies on networks are generally focused on electricity consumption as the use phase represents a significant part of their carbon footprint (around 80%). Malmodin and Lundén assessed the carbon footprint of ICT networks, up to 169 MtCO2e and 242 TWh in 2015. By synthesizing most of the available studies on this subject, Coroama estimates this consumption at 340 TWh in 2020. He points out the lack of coherence of the available results, for example, a divergence factor of 5–26 between bottom-up and top-down studies. The IEA estimates that energy consumption of networks was between 260 and 340 TWh in 2021 (Roussilhe et al., 2023).

<br>
<img src="img/S5-13.png" alt="Breakdown of impacts for a smartphone and a server" style="border: 1px solid gray;"><br>
<i>Breakdown of impacts for a smartphone and a server</i><br>

> The environmental footprint of end-user devices is generally assessed through LCAs. The global footprint is then obtained in a bottom-up fashion by estimating the number of devices of various types that are manufactured (based on sales number), in use (based on estimates of the average lifespan of such devices), and discarded in a given year, and multiplying these numbers by the corresponding impact of each type of device. Clément et al. performed a review of available LCAs of smartphones and tablets, which showed the importance of the manufacturing phase, and specifically that of integrated circuits (ICs), in the environmental impact of devices. There are no up-to-date data for computers and displays (Roussilhe et al., 2023). 

<br>
<img src="img/S5-14.jpg" alt="GHG emissions per smartphone by life cycle phase - Credits: Clement et al. 2020" style="border: 1px solid gray;"><br>
<i>GHG emissions per smartphone by life cycle phase - Credits: Clement et al. 2020</i><br>

<br>
<img src="img/S5-15.jpg" alt="Subcomponents production impact - Credits: Clement et al. 2020" style="border: 1px solid gray;"><br>
<i>Subcomponents production impact - Credits: Clement et al. 2020</i><br>


## Future trends

It is agreed that low-carbon transition operational scenarios should not be projected more than ten years ahead, as there are too many uncertainties regarding, among other things, the evolution of climate conditions or the evolution of the price of low-carbon technologies. The same applies to the digital sector, where it is difficult to predict the future equipment and efficiency of the equipment, or the emergence of new uses that are not yet known. Thus, Andrae and Malmodin project their estimates up to 2030. For some unknown reason, Belkhir projects his results to 2040, but the uncertainty, both climate-wise and technology-wise, in twenty years’ time disqualifies this estimate from the outset.

This uncertainty leads to a second methodological point: it is extremely risky to project the evolution of the sector based on historical data. As mentioned earlier, digital equipment and services are rapidly becoming more efficient at the moment, so it is not possible to estimate that their past footprint will evolve consistently. This goes in two directions: the relative power consumption (kWh or J / per operation) of a piece of equipment tends to decrease over time and does not remain constant over time. On the other hand, more complex or more efficient equipment can lead to an increase in the material footprint (manufacturing impacts). There are of course all the new uses that are not yet well modelled from an environmental point of view: AI, machine learning, IoT, blockchain, VR/AR, cloud gaming (Google Stadia, etc), etc.

Andrae estimates that the digital sector will account for 1,500 to 3,200 TWh of electricity consumption and 1,269 MtCO2e of emissions by 2030 (Freitag et al., 2021). Other researchers haven't proposed other estimates yet, but some expect that efficiency gains and substitution would allow the carbon footprint of the digital sector could be halved by 2030.

Charlotte Freitag's Lancaster team has summarised the various hypotheses in the scientific literature that may change the environmental footprint of the sector in the years to come:
* Efficiency gains continue steadily ;
* Renewable energy is increasingly replacing fossil fuels in the mix of large digital companies;
* Slowing efficiency gains reduce the growth of the sector and thus the growth of its emissions ; 
* Activation effects of digitalization (enablement effect) reduce the overall emissions of other sectors ;
* Increasing mutualisation of IT infrastructure (hyperscale, etc.) ; 
* Market saturation in terms of facilities and services ; 
* Reducing the weight of new equipment.

Among the assumptions that consider negative trends in the digital sector :
* Exponential growth of data is increasing the energy consumption of networks and data centres (growing infrastructure) despite efficiency gains ; 
* Emergence of new equipment-intensive (IoT) and data-intensive (cloud gaming, etc.) services ;
* Efficiency gains are slowing down ; 
* Rebound effects counterbalance efficiency gains at the aggregate level ; 
* Efficiency gains through digitalization support the development of the sector and therefore its footprint ; 
* Efficiency gains from digitalization increase the economic activity of other sectors and thus their environmental footprint ;
* Increasing the material intensity of new equipment.

In an unprecedented foresight exercise, the French Agency for Ecological Transition and the French Telecoms Regulator have attempted to project the environmental footprint of the digital sector in France in 2023 and 2050 using 4 scenarios. Estimates for 2050 are not recommended, but the median scenario for 2030 projects a 45% increase in the carbon footprint in 10 years.


<br>
<img src="img/S5-17.png" alt="Trends in 4 indicators of the environmental impact of the digital sector. - Credits: Lees Perasso et al. 2022" style="border: 1px solid gray;"><br>
<i>Trends in 4 indicators of the environmental impact of the digital sector. - Credits: Lees Perasso et al. 2022</i><br>

## Moving forward in the dark

Estimating the present and future environmental footprint of the digital sector is a delicate exercise, fraught with uncertainty. Rather than a precise figure, it is now more important to understand in which direction this footprint is heading and because of which factors. If the digital sector is to make the transition, we will need to identify the obstacles and levers to overcome the current situation. For the moment, all the evidence suggests that the footprint is increasing year on year, but not by the exponential growth predicted by some researchers. 

## Resources

### Syllabus
* Gauthier Roussilhe et al., [A long road ahead: a review of the state of knowledge of the environmental effects of digitization](https://www.sciencedirect.com/science/article/pii/S187734352300043X?via%3Dihub), 2023
* Charlotte Freitag et al., [The real climate and transformative impact of ICT: A critique of estimates, trends, and regulations](https://www.sciencedirect.com/science/article/pii/S2666389921001884), 2021 (Summary + Introduction)


### To go further
* Frederic Bordage et al., [Digital technologies in Europe: an environmental life cycle approach](https://www.greens-efa.eu/opinions/digital-technologies-in-europe/), 2021
* Etienne Lees Perasso et al., [Evaluation de l'impact environnemental du numérique en France et analyse prospective](https://librairie.ademe.fr/consommer-autrement/5226-evaluation-de-l-impact-environnemental-du-numerique-en-france-et-analyse-prospective.html), 2022
* Udit Gupta et al., [Chasing Carbon: The Elusive Environmental Footprint of Computing](https://arxiv.org/abs/2011.02839), 2021
* Steffen Lange et al., [Digitalization and energy consumption. Does ICT reduce energy demand?](https://www.sciencedirect.com/science/article/abs/pii/S0921800919320622), 2020
