# Collective project [1/3]


#### Activity: Starting collective project (2h)
	After being given instructions, each group works on its assignment




## Session goal

Make an assessment of climate and environmental risks (present and future), and transition policies of the city assigned to your group by the teacher.

## Role

You are civil servants being asked to deliver a recommendation for the city council and the mayor. Your analysis will help the city refuse, transform or accept the deployment of a given technology. 

**The first step of your analysis is to assess climate and environmental evolution, present and future associated risks in your city (present & future) and to sum up existing transition policies.** Other steps will be addressed in session 10 and 12.


## Instructions for the collective project

* Make groups of 4 people
* Each group will be assigned an European city
* Sum up transition policies in your assigned city (or/and at a greater scale if necessary)
* Assess climate and environmental evolution in your city (today (2020) to 2050)
* Assess climate and environmental risks in your city (today (2020) to 2050)
* Write a summary of your findings

### Information / data to look for

* Transition plan / Net zero carbon / low carbon roadmap of cities
* Scientific literature (using right keywords in search engine such as [Semantic Scholar](https://www.semanticscholar.org/) or equivalent)
* IPCC reports and tools
* External analysis from consultancy groups, NGOs, etc.
* Maps, geographical data, statistics, etc.

## Deliverable

Your assessment should take the form of a note (4 pages maximum). Besides text, you can add different media within your summary (interactive maps, graphs, pictures, etc.).





