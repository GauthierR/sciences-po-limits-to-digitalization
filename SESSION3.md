# Session 3 - Deploying digital infrastructures

## Moving heaven, water and earth

Understanding the general functioning of digital infrastructures is one thing, but they don't appear overnight. Before installing a data center, telecommunication networks or electronic component factories, physical, political, environmental and economic conditions must be negotiated. Just like maintenance, this phase reveals in part what it really costs to develop these infrastructures. Thus, this deployment is not trivial and must be understood by both policy makers and the citizens who will live with these infrastructures.

> Infrastructure is an idea, a vision or an ideal, but it is also a practice, a commitment and a long term endeavor. (Ribes, Universal informatics: Building cyberinfrastructure, interoperating the geo-
sciences, p.299)

## Data centers

Data centers are like any industrial complex and need to meet several conditions to be deployed and used. For instance, the location choice of a data center is influenced by the following characteristics: 
* privileged access to the electricity grid (source station)
* privileged access to internet fibre network (backbone)
* low natural risks (eruptions, floods, etc.)
* access to cheap land
* low cost or preferential electricity rate
* tax benefits or tax reductions
* potentially access to a water network

Beyond servers and network equipment, it means that inside a data center you will find power lines, going to a transformer and also to batteries, along with fuel tanks and power generator to take over in case of power failure. The transformer will send power to IT equipment (servers, etc), to the cooling unit and to the rest of the facility. Like any industry, data center operators need to negotiate with local authorities to establish itself in the local area and its infrastructures.

<br>
<img src="img/S2-02.jpg" alt="What does a data center need and what is inside?" style="border: 1px solid  gray;">
<i>What does a data center need and what is inside? – See high-res [here](https://gauthierroussilhe.com/en/resources/setting-up-a-data-centre)</i><br>

Submitting a data centre project to a local authority is generally a lengthy exercise, and one that tends to remain discreet in order to avoid the interests of competitors and local opposition. Here you can browse Amazon Web Services' data centre project in the town of Gilroy [here](https://cityofgilroy.org/DocumentCenter/View/12690/AWS-GDC-Project-Description-and-Figures?bidId=105). Data center projects are typically designed in phases, increasing capacity and demand of the infrastructure with each step of the phasing. In that case, Amazon is securing land (438,500 square feet) and is giving a list of the infrastructure and equipment need for the project : backup generation, battery systems, their own substation, a water pipeline, security fencing and protection from storms. 

A data centre is never just about servers; this type of industry involves a whole range of local and national players and relies heavily on access to existing infrastructure. However, the scale of the tech giants' new data centre projects means that they now have to create their own infrastructure for both energy and water supplies. This does not mean, however, that this eases the burden they place on an area.

### Power availability in Ireland

Like any industry, if data centers are too concentrated in the same area the power available on the grid becomes more scarce. For instance, Ireland has attracted many tech companies to set up in their territory, notably thanks to large financial advantages. The Irish Central Statistics Office estimated that data centers accounted for 11% of national electricity consumption in 2020 and 14% in 2021. The national electricity supplier estimates that at this rate data centers will represent 33% of Ireland's electricity consumption by 2030. This growth is of course unsustainable for the energy provider Eirgrid and the latter has notably [warned](https://www.datacenterdynamics.com/en/analysis/dublin-and-data-centers-the-end-of-the-road/) that it will no longer supply electrical power to new data center projects in Dublin until 2028. The place and importance of data centers is giving rise to important societal debates in Ireland and presages a larger problem of accounting between economic development, infrastructure management and climate policy.

<br>
<img src="img/S3-13.png" alt="Data Centres Metered Electricity Consumption 2022" style="border: 1px solid  gray;">
<i>
Data Centres Metered Electricity Consumption 2022 - Credits: Irish Central Statistics Office</i><br>


This energy demand is also difficult to reconcile with the climate commitments which imply a reduction in energy consumption in absolute terms by 2050. For example, France must reduce its final energy consumption from 1600 TWh in 2020 to 900 TWh by 2050. However, the French energy network operator (RTE) estimates that electricity consumption by French data centres will increase threefold between 2020 and 2050. We still need to be sure that these infrastructures are worth the investment, enabling us to decarbonise other sectors of the French economy.

### Water stress / consumption in the United States

Larger (hyperscaler) data centers typically use evaporative water cooling, especially in the western part of the United States. However, these areas experience varying degrees of water stress. Some of these data centers may require a significant amount of water in relation to territorial capacities and may potentially create conflicts of use. Beyond the cooling techniques (direct water footprint), the origin of the electricity is a major problem. Indeed, water is needed to produce electricity in power plants equipped with steam turbines or hydroelectric dams. On average the water intensity for electricity generation for 2015 in U.S.A. was 2.18 liters per kilowatt hour (Mytton 2021). In this sense, water stress can affect the production of electricity but also data centers that use water cooling. There are other ways of cooling with water, including adiabatic cooling which uses much less water. Scaleway's DC5 data center in northern Paris is equipped with this type of technology.

>Our bottom-up approach reveals one-fifth of data center servers direct water footprint comes from moderately to highly water stressed watersheds, while nearly half of servers are fully or partially powered by power plants located within water stressed regions. (Siddik et al., 2021)


<br>
![Data center](img/S3-04.jpg)<br>
<i>Example of evaporative cooling in a Google's data center (Credits: Google)</i><br>

It seems complicated to maintain data centers in areas of high water stress over the medium to long term. It's hard to know today how operators are taking this into account despite warnings from various professional organizations.

> Google and Microsoft are leading in renewable energy, but even they are secretive about their water resource management. It is easy to criticise Amazon’s lack of transparency but they are not alone the entire data centre industry suffers from a lack of transparency. (Mytton, 2021)

<br>
<img src="img/S3-03.png" alt="The environmental footprint of data centers in the United States" style="border: 1px solid  gray;">
<i>The environmental footprint of data centers in the United States, Siddik et al. 2021</i><br>

> [...] Many of the watersheds in the Western US exhibit high levels of water stress, which is exacerbated by data centers direct and indirect water demands. Combined, the West and Southwestern watersheds supply only 20% of direct water and and 30% indirect water to data centers, while hosting approximately 20% of the nation’s servers. Yet, 70% of the overall WSF occurs in these two regions (figure 3(B)), which indicates a disproportionate dependency on scarce waters in the western US.
(Siddik et al., 2021)

Water stress is set to accelerate in many parts of the world, and water supply and management issues will become increasingly pressing for many data centres. As a result, the construction of new data centres is increasingly contested, as was recently the case in Talavera de la Reina, Spain, where Meta planned to build a €1 billion ($1.1 billion) data centre. Meta expects the facility to use about 665 million liters (176 million gallons) of water a year, and up to 195 liters per second during "peak water flow," according to a technical report (see [Bloomberg's article](https://www.bloomberg.com/news/articles/2023-07-26/extreme-heat-drought-drive-opposition-to-ai-data-centers#xj4y7vzkg)). Meanwhile, the central region of Castilla La Mancha, which produces a quarter of all Spanish grain, is expected to lose 80% to 90% of this year’s harvest (2023), and water restrictions loom large. The same is happening in London, where Thames Water is considering measures to cut down the water used by some datacenters according to [the Register](https://www.theregister.com/2023/07/27/thames_water_to_datacenters_cut/).

A final critical point is confidence in the water consumption forecasts provided by data centre operators. The case of a [Microsoft data centre in Amsterdam](https://www.datacenterdynamics.com/en/news/drought-stricken-holland-discovers-microsoft-data-center-slurped-84m-liters-of-drinking-water-last-year/) is illustrative: Microsoft had forecast annual water consumption of between 12 and 20,000 m³. In reality, however, this infrastructure consumed 84,000 m³ of water in 2021, while an independent expert hired by Microsoft re-estimated the centre's annual water consumption at around 100,000 m³. Microsoft explains that the data centre's hydraulic cooling circuit is only opened when outside temperatures exceed 25°C. However, for its calculations, the company used weather data from the year before it moved in, during which Hollands Kroon only had six summer days above 25°C. In 2021, there were 22. And this year, the record is likely to be even worse. Hence the big difference between estimates and reality.

## Telecommunication networks

The telecommunication networks are rarely put forward yet it is a keystone of the data transfer but also the most fragile part. Most post-industrial countries are launching major projects to deploy fiber networks throughout their territory with varying degrees of success. Likewise, new generations of communication protocols are being implemented such as 5G and new services are entering the market like Starlink. The evolution of digital services as we know them is linked to the evolution of networks, which is why it is particularly important to understand what it means to deploy and maintain them.

### Deploying and maintaining networks in France

Deploying networks requires a great deal of civil engineering effort to integrate them into the existing infrastructure or to build new routes. It is a long job that requires many people on the ground. In many respects, the deployment of fiber in France is the largest infrastructure project on the national territory since the deployment of telephone and electricity networks.

<br>
<img src="img/S3-11.jpg" alt="Deploying fiber and cellular networks (Credits: Jean-Pierre Sageot (aerial fiber), Greenpose (underground fiber))" style="border: 1px solid  gray;">
<br>
<i>Deploying fiber and cellular networks (Credits: Jean-Pierre Sageot (aerial fiber), Greenpose (underground fiber))</i><br>

Deploying fiber with a FttH (Fiber-to-the-Home) model, which means one fiber distribution termination point (DTP) per home, is very expensive. For example, in the most expensive areas, rural areas, the deployment of fiber in Ardèche and Drôme will cost 597 million euros to install 311,000 DTP. As these areas are not profitable for private operators, it is usually the public institutions (state, region, department, etc.) that pay for the deployment and lease it to a private operator. Even with this massive deployment, 3% of homes are not connectable by fiber and are instead eligible for mobile or satellite offers. This example puts into perspective the idea that all countries on Earth could be connected directly to fiber networks. This requires enormous capital that is generally out of reach for many countries in the Global South. Access to cellular or satellite networks may then be a better option, but this affects the equality of connection access and the nature of the digital services that can be used.

<br>
<img src="img/S3-12.png" alt="The cost of deploying fiber networks in Ardèche-Drôme - Credits: ADN" style="border: 1px solid  gray;"><br>
<i>The cost of deploying fiber networks in Ardèche-Drôme - Credits: ADN</i><br>


However, like any infrastructure it requires care and vigilant maintenance. Without the tens of thousands of technicians who maintain networks every day, we can assume that our Internet access would not last long. Sometimes the equipment is damaged by accident and/or because its location puts it in danger (because it depends on the constraints of other networks). In countries like France, the deployment and maintenance of fiber networks is particularly difficult because large telecom operators use many levels of subcontractors, sometimes poorly trained or with untenable objectives, with a very patchy access to information. For example, a technician may unplug a subscriber from a fiber outlet to connect his own customer.

<br>
![Messy sharing point](img/S3-09.jpg)<br>
<i>Area sharing point 'poorly maintained' (Credits: Unknown)</i><br>

Acts of malice on the network are also significant, even if their origins are difficult to trace. A large number of incidents seem to be caused by unpaid subcontractors or conflicts between different subcontractors. Malicious acts may also be committed for political or ideological reasons (see: [all disruptions on French telecom networks in 2021](https://reporterre.net/embedded/map/sabotage-5g-src/)). However, these acts are a minority compared with the biggest source of network damage and breakdowns: civil engineering. Crews often don't know exactly where the cables and pipes are in the ground when they are digging, so they pull them out or damage them. Accidents of this kind can happen almost daily on a national network. For example, in Frankfurt in February 2023, while digging at a depth of 5 metres, workers hit a critical cable on the network on which Lufthansa's IT system depends. As a result, Lufthansa's flights were cancelled for almost a day (See [Deutsche Telekom's post](https://twitter.com/deutschetelekom/status/1625587220785598464) and [Lufthansa's post](https://twitter.com/lufthansa/status/1625807325285040128)). On 25 July 2023, during work on a new tram line in Brest, France, site crews ripped out a cable, [causing 15,000 subscribers to be cut off from the internet for a week](https://france3-regions.francetvinfo.fr/bretagne/finistere/brest/panne-geante-a-brest-15-000-foyers-sans-internet-pour-une-semaine-2816435.html).


<br>
<img src="img/S3-14.jpg" alt="Cable wrapped around a drill head near Voiron, France - Credits : Julien Ohayon" style="border: 1px solid  gray;"><br>
<i>Cable wrapped around a drill head near Voiron, France - Credits : Julien Ohayon</i><br>


### Deploying submarine cables worldwide

To ensure intercontinental and inter-oceanic links, submarine cables have been massively deployed. These are laid in the seabed by specialized ships. The cables are connected to the terrestrial network in coastal facilities called "landing points". According to TeleGeography: "as of late 2021, there are approximately 436 submarine cables in service around the world [...] and over 1.3 million kilometers of submarine cables in service." (4 times the distance between the earth and the moon). On average, the lifespan of a submarine cable is between 20 and 30 years, but it can be replaced earlier by more efficient and economically viable cables. Content providers (Facebook, Google, Amazon, Netflix, etc.) are the [main customers of submarine cable installers](https://blog.telegeography.com/content-providers-binge-on-global-bandwidth) as they are also the biggest consumers of bandwidth.

<br>
<img src="img/S2-09.jpg" alt="Submarine cables - Pictures" style="border: 1px solid  gray;"><br>

Most accidents on underwater cables are due to boat accidents, commonly the cable is caught in the nets of a fishing boat. Natural disasters can also affect cables (earthquake, volcanic eruption, underwater landslide, etc.). On average, there are over [100 cable faults](https://blog.telegeography.com/what-happens-when-submarine-cables-break) each year.

<br>
<img src="img/S2-06.jpg" alt="A simplified view of submarine cables" style="border: 1px solid  gray;"><br>
<i>A simplified view of submarine cables – See high-res [here](https://gauthierroussilhe.com/en/resources/submarines-cables)</i><br>


## Industry

The digital sector relies on a large number of materials and components produced by the electronics industry. As digitisation and the demand for components increase, so does the need to create new factories, whether for electronic chips or even the end-of-life processing of all this equipment. As we will be looking at the semiconductor industry in depth in session 7, we will just touch on the issue of end-of-life processing.


### Recycling facilities for electronic waste in Belgium

There are few industries specialized in the recycling of electronic waste. Umicore's Hoboken site recovers some 17 metals from over 200 complex input streams from all around the globe. 400,000 t of feed-materials are treated annually (2017) and this plant is one of the world’s largest precious metals recycling facilities with a capacity of over 50t PGMs (Platinum Group Metals), over 100t of gold and 2400t of silver (2007 numbers). The Hoboken plant is profitable due to the recovery of gold, silver and platinum which are on average 7 times more concentrated in e-waste than in a conventional mining site. Despite this ability to recover some metals from the complex waste stream, the mineral demand is so large and diverse that we cannot function without extracting more and more material. For instance, we have never recycled copper so well, yet we have never extracted as much as we do today.

> We can explain the recycling rate of metals by their price, their concentration in the product, and the relative metal concentration between products and primary deposits. The marginal response of the recycling rate to price seems to be low, regardless of the specification and the model used. At best, tripling price corresponds to an increase of +7.9 point of percentage of recycling rate. This finding is in line with the sparse literature on metal recycling flow. [...] an addition of one percentage point of metal concentration is correlated with a rise in the metal recycling rate of 2.52 percentage points. (Fizaine, 2020)

<br>
![Hoboken](img/S3-06.png)<br>
<i>Umicore electronic waste recycling facility in Hoboken, Belgium</i><br>

Processing the waste requires energy, water and many chemicals to separate the metallic elements. Normally, the wastewater is partly recycled and a third is discharged into the river next to the plant. However, the plant is tied to an environmental justice movement because of the pollution it is said to cause. Numerous NGOs [have documented](https://ceecec.net/case-studies/ecological-debt-environmental-justice-in-belgium/) for years the pollution originating from the plant, the contaminations and the cancer cases that were allegedly caused by Umicore.

Without collection and treatment, electronic waste is generally sent through informal or illegal channels to countries in Africa, South America or South East Asia. The metals and materials they contain are then extracted in conditions that are dangerous for human health and ecosystems. Today, it is estimated that 80% of electronic waste goes through informal channels. 

## Not so immaterial

The information economy, and the digital infrastructure on which it is based, is far from immaterial. These infrastructures require very important material, political and economic means to be deployed and maintained over long period. Similarly, these infrastructures depend on environmental conditions that seem hard to maintain depending on countries and territories where we look at. Digital infrastructures, although not very visible, influence the way digital services are thought and used. But can the conditions necessary for their existence be maintained in an increasingly unstable world? 


## Resources

### Syllabus
* Fanny Lopez, Maximilien Gawlik, Lisa Gaucher, [Data centers: anticipating and planning digital storage](https://en.institutparisregion.fr/resources/publications/data-centers-anticipating-and-planning-digital-storage/), 2021
* TeleGeography, [Submarine Cable Map 2022](https://submarine-cable-map-2022.telegeography.com/), 2022



### To go further
* Abu Bakar Siddik et al., [The environmental footprint of data centers in the United States](https://iopscience.iop.org/article/10.1088/1748-9326/abfba1), 2021
* David Mytton, [Data centre water consumption](https://www.nature.com/articles/s41545-021-00101-w), 2021
* Steven Gonzalez Monserrate, [The Cloud Is Material: On the Environmental Impacts of Computation and Data Storage](https://mit-serc.pubpub.org/pub/the-cloud-is-material/release/1), 2022
* Mél Hogan, [Data flows and water woes: The Utah Data Center](https://journals.sagepub.com/doi/10.1177/2053951715592429), 2015
* Nicole Starosielski, [The Undersea Network](https://books.google.se/books?hl=fr&lr=&id=NtGqCAAAQBAJ&oi=fnd&pg=PT7&dq=Starosielski&ots=IgaTHZx7Pa&sig=MLY4pCg4DrMhPMwc9fYmkpKQ1u4&redir_esc=y#v=onepage&q=Starosielski&f=false), 2015
* Nicole Starosielski, [ Pipeline Ecologies - Rural Entanglements of Fiber-Optic Cables](https://www.taylorfrancis.com/chapters/edit/10.4324/9781315794877-4/pipeline-ecologies-nicole-starosielski), 2016
* Christian Hagelüken, [Improving metal returns and eco-efficiency in electronics recycling](https://www.researchgate.net/profile/Christian-Hagelueken/publication/241271783_Improving_metal_returns_and_eco-efficiency_in_electronics_recycling/links/54c9f4750cf298fd26274ea0/Improving-metal-returns-and-eco-efficiency-in-electronics-recycling.pdf), 2006
* Florian Fizaine, [The economics of recycling rate: New insights from waste electrical and electronic equipment](https://www.sciencedirect.com/science/article/pii/S0301420720300805), 2020.
* Bloomberg, [Thirsty Data Centers Are Making Hot Summers Even Scarier](https://www.bloomberg.com/news/articles/2023-07-26/extreme-heat-drought-drive-opposition-to-ai-data-centers#xj4y7vzkg)
* The Register, [Thames Water to datacenters: Cut water use or we will](https://www.theregister.com/2023/07/27/thames_water_to_datacenters_cut/)
* Data Center Dynamics, [Drought-stricken Holland discovers Microsoft data center slurped 84m liters of drinking water last year](https://www.datacenterdynamics.com/en/news/drought-stricken-holland-discovers-microsoft-data-center-slurped-84m-liters-of-drinking-water-last-year/)
* Oregon Live, [Google’s water use is soaring in The Dalles, records show, with two more data centers to come](https://www.oregonlive.com/silicon-forest/2022/12/googles-water-use-is-soaring-in-the-dalles-records-show-with-two-more-data-centers-to-come.html)