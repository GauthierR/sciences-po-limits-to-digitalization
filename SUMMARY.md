# Summary

* [Introduction](README.md)

## Sessions
* [S1 - Crossing perspectives](SESSION1.md)
* [S2 – Understanding digital infrastructures](SESSION2.md)
* [S3 – Deploying digital infrastructures](SESSION3.md)
* [S4 – The material conditions of ICT](SESSION4.md)
* [S5 – The environmental footprint of ICT](SESSION5.md)
* [S6 – The climate risks on ICT](SESSION6.md)
* [S7 - *Collective project*](SESSION11.md)
* [S8 – Case study: Chips in Taiwan](SESSION7.md)
* [S9 – Modelling indirect effects](SESSION8.md)
* [S10 - *Collective project*](SESSION13.md)
* [S11 – Risk assessment and critical thinking](SESSION9.md)
* [S12 - *Project presentation*](SESSION14.md)


## Resources