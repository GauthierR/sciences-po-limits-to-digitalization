# Session 4 - The material conditions of ICT

## Browsing through a global footprint

To produce the phenomenal quantity of components, devices and digital equipment used today, it is necessary to ensure a constant and globalized flow of materials. This flow links the mining areas to the manufacturing plants, and these plants to recycling and waste facilities, but could it be maintained? What are the material demands of the digital sector and how do they relate to the general demand? Furthermore, what is the social and environmental cost of extracting these resources?

<iframe width="100%" height="450" frameborder="0" title="Felt Map" src="https://felt.com/embed/map/Global-mining-footprint-FhngephpTdWu9ChFO4rwIQA?loc=-18.87,-67.57,5z"></iframe>


## Current and future demand for minerals

The extraction of metals requires the opening of a mine, either underground or open. Mines are opened because the industry requires more raw materials or projects to do so. Indeed, investors will generally only fund a mining operation if there is demand and profitability. It is therefore demand that drives the production of mineral raw materials: supply adapts to demand. However, increasing construction and new industries have greatly increased demand.

<br>
![IRP](img/S4-05.jpg)<br>
<i>Jubilee Mine - 6,500 tonnes of copper - Credits: Dillon Marsh</i><br>

### Understanding our current reserves

Human societies, mainly industrial, consumed 210 megatons (Mt) of minerals worldwide in 1900, then 6.5 gigatons (Gt), or 6500 megatons, in 2009 . During this period of a little more than a century, the population has multiplied by 4 while the consumption of ores and industrial minerals has multiplied by 30. The latter is mainly due to specific societies and social classes. As such, the 30-fold increase in mineral consumption is not attributable to humanity as a whole, but to certain societies in certain territories over a certain period of time. 

<br>
<img src="img/S4-12.png" alt="Evolution of world primary metal production" style="border: 1px solid gray;"><br>
<i>Evolution of world primary metal production - Credits: BRGM</i><br>

When demand emerges from the industrial and financial sectors, supply does not instantly line up as if it were a matter of releasing stocks. It takes several decades from the beginning of geological exploration, the first drillings, the opening of the mine, to the exploitation and the extraction of the first ton of ore. In the case of exploration in an already known area, this deployment time can be quite "fast", between 10 and 15 years. In the case of an exploration in a little known area, it is necessary to count between 20 and 30 years. It is important to understand that if a demand for a metal emerges in 2020 and investments are released for geological exploration at the same time, then the first tons extracted (supply) to meet this demand will be available between 2030 and 2050. So anticipation is key in the mining sector.

Understanding how to qualify the importance of mineral resources and reserves is not straightforward. There is a clear distinction between two terms: resources on the one hand and reserves on the other. In geology, "resources" correspond to the concentration of a liquid, solid or gaseous substance in the earth's crust. Thus the definition of resources varies greatly according to geological knowledge and the measurement tools available. As exploration and extraction techniques become more sophisticated, the resources discovered or extractable may increase. The more certain we are of an ore concentration in the ground, the more economically qualified the resource is. We then speak of "inferred" resources for the least well identified resources, "indicated" resources and "measured" resources for those that are better known. There are also "unidentified" resources, which are an estimate based on the mathematical probabilities provided by previous explorations. 

Reserves correspond to known concentrations of ore that can be economically and technically exploited at a given price at a given time. It is therefore the market price, technological progress in extraction methods and processing costs, geological concentration, quality and depth of the vein that indicate how much of the measured resources become exploitable reserves. There are several types of reserves, two of which are prominent: the reserves base includes all proven resources (measured and indicated) that are mineable by current mining practices. The reserves base includes reserves that are economically recoverable at the time of evaluation (economic reserves), those that are close to being economically recoverable (marginal reserves), and those that are not currently economically recoverable at all (sub-economic reserves). It is from the reserve base that the global reserve of a resource is calculated. The so-called "reserves" are the part of the reserve base that is technically, geologically and economically exploitable at the time of the evaluation. This does not mean that all reserves are exploited, but rather that they could be exploited at the time.

<br>
<img src="img/S4-04.png" alt="Difference between resources and reserves" style="border: 1px solid gray;"><br>
<i>Difference between resources and reserves</i><br>


The definition of resources and reserves is therefore a complex exercise that depends on geological factors (location, concentration, quality, depth of the deposit), scientific and technological factors (state of knowledge of exploration, drilling, production, design of the necessary machinery), and economic factors (price on the raw material markets, cost of extraction, cost of the energy required for extraction, investment and operating costs). Other factors also come into play, such as the geopolitical factor: resources are not equally distributed in the earth's crust, some countries have "richer" soils than others. There are also social and environmental factors: the opening of a mine and the cost of extraction also depend on the social standards (protection of personnel, salary policy, etc.) and environmental standards (pollution and depollution standards, water use, etc.) of the country where the operation is located. The criticality of a resource, rather than its depletion, is therefore relative to all these criteria. 

### Future demand

There is a general consensus that global mineral demand is going to increase very rapidly, especially with technologies related to the transition (electric vehicles, renewable energy). In a sustainable development scenario, the IEA estimates that we need to increase our mineral consumption by a factor of 4 between 2020 and 2040. At a time when the most abundant deposits are already being exploited and discoveries of new deposits are slowing down, it seems impossible to meet this demand.

<br>
<img src="img/S4-03.png" alt="Growth scenario for mineral demand, International Resources Panel 2019" style="border: 1px solid gray;"><br>
<i>Growth scenario for mineral demand, International Resources Panel 2019</i><br>

<br>
<img src="img/S4-02.png" alt="Mineral demand for clean energy technologies by scenario - Credits: IEA 2021" style="border: 1px solid gray;"><br>
<i>Growth scenario for mineral demand by technology to achieve net-zero scenario - Credits: IEA 2021</i><br>

<br>
<img src="img/S4-02-1.png" alt="Revenue from production of coal and selected energy transition minerals in the SDS - Credits: IEA 2021" style="border: 1px solid gray;"><br>
<i>Growth scenario for mineral demand by technology to achieve net-zero scenario - Credits: IEA 2021</i><br>

The energy transition relies on technologies and accelerated electrification that are very metal intensive. The production of batteries and electric vehicles represent a major part of this mineral demand. It seems unlikely that we will have to extract so much mineral at this scale and speed.

We could theoretically continue to push back the limits of known reserves and not experience metal depletion as long as we have an adequate technical knowledge, an attractive resource price, and sufficiently low exploitation costs (and therefore energy costs). Under these conditions, we could go and exploit deposits that are difficult to access, such as non-conventional deposits and deep-sea deposits. However, these conditions will be increasingly difficult to meet, despite an increase in demand, the best deposits have already been exploited and the yields of the deposits are falling. The engineer Philippe Bihouix describes the following vicious circle: ores are less and less concentrated, so more and more energy is needed to extract them, energy equipment uses more and more metals, so it is more and more difficult to produce energy and extract new metals. We can assume that we will have a criticality problem due to an energy problem, a water problem, a social problem and an environmental problem before we have a resource availability problem. It remains to be seen how long we will be able to pay this price and whether we really want to pay this price. Alain Geldron, national expert in raw materials believes that "the depletion of mineral resources before the end of this century is very unlikely for most mineral raw materials, but shortages over a long period of time are to be expected, if new deposits are not exploited quickly enough." 

> [...] provides arguments for two classically opposed views of the future of mineral resources: an unaffordable increase in costs and prices following the depletion of high quality deposits or, on the contrary, a favourable compensation by technological improvements. Both visions are true, but not at the same time. After a period of gains in energy and production costs, it seems that we are now entering a pivotal period of long-term increasing production costs as we approach the practical minimum energy and thermodynamic limits for several metals. (Vidal et al., 2021)


<br>
<img src="img/S4-07.jpg" alt="Annual demand for Cu, Li, Ni, and Co calculated for the evolution of infrastructure in the reference technology" style="border: 1px solid gray;"><br>
<i>Annual demand for Cu, Li, Ni, and Co calculated for the evolution of infrastructure in the
reference technology (RTS) (Left panel) and the “Beyond 2 ° C” scenario (B2DS) (Right panel) of the
International Energy Agency - Credits: Vidal, 2022</i><br>


## Social and environmental conflicts

Discovering more deposits and exploiting them is not without consequences. The mining industry is one of the most polluting on Earth due to the use of toxic products, and its massive consumption of energy and water, which also ravages biodiversity and human health. Contrary to what the industry claims, there is no such thing as a clean or green mine. From an environmental point of view, the only thing to do is to limit the number of mines and to make the extraction conditions less dirty.

The opening of a mining site is very often a source of conflict. First, there is the issue of land use and land transformation. If the deposit is located in an inhabited area or in an area of high biodiversity, the project is likely to be conflictual. Second, there is the issue of resources needed to operate the mine. For example, large quantities of water are needed for the lithium salars in Chile and create conflicts over water use with local populations. Third, there is the question of pollution linked to the mine: air pollution, water pollution, toxicity for humans, wildlife, voluntary discharges, etc... Thus, the mining sector faces many legitimate challenges from local populations. Unfortunately, this sector has the highest number of killings of environmental activists according to Scheidel et al.


> The huge expected increase in mineral resources consumption and primary production will increase conflicts and social opposition. The surface occupied by mines and quarries at the world level has been estimated to be about 400,000 km2 ; it could be doubled by 2050 and quadrupled by 2100. The embodied water consumption and other environmental impacts are expected to follow the same trends. (Vidal et al., 2021)

<br>
<img src="img/S4-09.png" alt="Environmental conflicts registered in the EJAtlas and occurrence of assassinations of environmental defenders across conflict types" style="border: 1px solid gray;"><br>
<i>Environmental conflicts registered in the EJAtlas and occurrence of assassinations of environmental defenders across conflict types - Credits: Scheidel et al. 2020</i><br>

Moreover, the countries that consume the most mineral resources are generally not those that extract them. This means that pollution, water conflicts and other social and health consequences are not suffered by those who consume the end products. Thus the environmental consequences of the phenomenal increase in mineral demand will be less felt by post-industrial countries. The increase in mineral demand will result in the opening of new mining operations where social unrest may increasingly intensify due to the well known polliutions associated with the mining industry. Potentially, this will reinforce the "need" for more authoritarian regimes to quell local protests in order for mining to expand.

The issue of resources thus goes well beyond environmental issues and brings us directly back to the social, health and political issues related to this sector. A priori, we will not be able to meet the mineral demand envisaged in the future without making the conditions of exploitation more and more unbearable. 

<br>


## Material resources for ICT


#### Activity: Dismantling (20min)
	Take apart the device provided by the teacher and try to determine 
	the number of metals in the device.

<br>
<img src="img/S4-13.png" alt="Weight of metals in a Fairphone 4" style="border: 1px solid gray;"><br>
<i>Weight of metals in a Fairphone 4 - Credits: Fairphone</i><br>

In the digital sector there are two categories of metals, those used in large quantities for their structural functions (telecommunication networks → copper, aluminum, some steels) and metals used in small quantities for their exceptional properties in the high-tech industry (small metals and precious metals) . In total, the digital sector solicits more than 60 metals in very diverse volumes and more than half of these metals depend on the extraction of other metals. Indeed, some small metals demanded by the digital sector do not represent a sufficiently large volume of demand to justify the opening of a dedicated mine, so they are extracted in mines whose profitability depends on the massive extraction of large metals. For example, gallium depends on the extraction of aluminum bauxite and germanium is only recoverable from zinc ore. This interdependence can be problematic because it implies that an increase in demand for a small metal will not lead to an increase in supply because it would require an increase in production of the large metal on which the small metal depends. If the demand for germanium increases, then zinc production must be increased, and to justify this increase in production, it must be economically attractive, so the demand for zinc must increase. In short, the supply of small metals and precious metals will not always match the demand because of their co-dependence. 

<br>
<img src="img/S4-08.png" alt="Co-dependence between minerals" style="border: 1px solid gray;"><br>
<i>Co-dependence between minerals</i><br>

So what are the differences in production between large metals and small and precious metals? When it comes to large metals like copper, aluminum, iron or tin for example, their consumption is also increasing. According to the International Copper Study Group, the total consumption of copper in the world amounted to 24 million tons in 2017. The share of consumption related to the use of copper as an electrical conductor reached almost 60%, "that is, about 11 million tons for the generation, distribution and transmission of electricity, and 3 million tons for the subset of electrical and electronic equipment to which digital belongs," as Liliane Dedryver reminds us for France Stratégie. The governemental think tank estimates "that one ton of copper is needed to produce 250,000 smartphones and tablets, the 1.4 billion new smartphones sold worldwide in 2017 (including 20 million in France) required the consumption of 5,600 tons of copper (including 80 tons for France)." 

<br>
<img src="img/S4-10.png" alt="Volume of metals in standard electronic devices" style="border: 1px solid gray;"><br>
<i>Volume of metals in standard electronic devices - Credits: Hagelüken and Corti, 2010</i><br>

On the side of small metals the volumes mined are largely different. For example, the annual production of tantalum was estimated at 1,800 tons in 2017 and 2018 . At least half of the production of this metal was used for the manufacture of electronic capacitors. These capacitors have notably enabled the miniaturization of digital equipment. The manufacture of consumer electronics is driving the majority of the production of this metal and leads to an estimated annual growth rate of 5.8% . Other metals such as gallium and germanium are used for their properties in the manufacture of semiconductors. Their annual production is estimated at 320 and 106 tons respectively. Less than one gram of each of these metals is used in the manufacture of a smartphone. Although the volume produced is small, digital equipment accounts for the bulk of the demand. 

<br>
<img src="img/S4-11.png" alt="Weighted distribution of metals per components/devices" style="border: 1px solid gray;"><br>
<i>Weighted distribution of metals per components/devices - Credits: Hagelüken and Corti, 2010</i><br>

In the face of ever-increasing material consumption, recycling of e-waste is still not encouraged, both by the design of the equipment and by the level of investment required for a recycling center. The metals present in digital equipment are difficult to recover and recycle because they are present in too small quantities or because they are used in an alloy and cannot be separated. Recycling industries have to invest much more to recover metals than other industries: "Florian Fizaine cites the example of the company UMICORE: "One billion dollars has been invested in Umicore's recycling and refining plant operating in Belgium. This plant extracts 30 tons of gold, 37 tons of platinum group metals, 1,000 tons of silver and 68,500 tons of other metals per year from scrap. This makes it the third largest gold mine in the world [...] By comparison, a paper recycling plant only requires an investment of 30 to 50 million dollars" (France Stratégie). In the end, recycling is not a long-term solution and the crux of the problem is the increase in consumption which leads to an increase in extraction, whether we have an efficient recycling chain or not. A constant growth of the couple production/consumption will inevitably be linked to an increase of pollution in water, air and soils affecting the ecosystems and communities where the extraction and waste disposal activities are located.

## Towards a scarcer world

The increase in demand for resources, particularly minerals, compared to the current state of the mining sector indicates that shortages are coming in several metals. If mining projects accelerate then environmental, social and health damage will increase together. Digital technologies increase the pressure on small and rare metals while including them in products that are not designed for resource recovery. Recovery of small and rare matelas through recycling is now extremely limited except for a few high-value metals like gold and platinum. We've just touched on the mineral resource issues here, but the material foundation of digital technology is made up of a large number of critical resources.

It would be naive to think about the future of digital technologies without taking into account the question of resources, particularly mineral resources. The future of the digital sector will be shaped by cyclical shortages of several metals and the resulting shockwaves in production chains. While the digital sector has historically been built on an abundant material base, this base is rapidly being eroded. Maintaining the current trajectory of development of the digital sector implies amplifying the social and environmental disasters linked to the mining sector. Under these conditions, there will be no clean or fair digital transition. One should even ask whether it is desirable.

## Resources

### Syllabus
* Dillon Marsh, [For What It's Worth](http://dillonmarsh.com/fwiw.html), 2018
* Judith Pigneur, [The Potential for Responsible Sourcing in the Magnet Value Chain: Strategies for the Future](https://www.fairphone.com/wp-content/uploads/2020/12/Rare-Earth-Presentation.pdf), 2020
* IEA, [The Role of Critical Minerals in Clean Energy Transitions](https://www.iea.org/reports/the-role-of-critical-minerals-in-clean-energy-transitions44), 2021


### To go further
* Olivier Vidal et al., [Modelling the Demand and Access of Mineral Resources in a Changing World](https://www.mdpi.com/2071-1050/14/1/11/htm), 2021
* US Geological Survey, [Mineral Commodity Summaries 2023](https://pubs.usgs.gov/periodicals/mcs2023/mcs2023.pdf)
* Florian Fizaine, [The economics of recycling rate: New insights from waste electrical and electronic equipment](https://www.sciencedirect.com/science/article/abs/pii/S0301420720300805), 2020
* Liliane Dedryver, [La consommation de métaux du numérique : un secteur loin d’être dématérialisé](https://www.strategie.gouv.fr/sites/strategie.gouv.fr/files/atoms/files/fs-2020-dt-consommation-metaux-du-numerique-juin.pdf), 2020 [FR]
* Arnim Scheidel et al., [Environmental conflicts and defenders: A global overview](https://www.sciencedirect.com/science/article/pii/S0959378020301424), 2020
* SystEx, [Controverses minières - Pour en finir avec certaines contrevérités sur la mine et les filières minérales - Volet 1](https://www.systext.org/node/1785), 2021 [FR]
