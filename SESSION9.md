# Session 9 - Risk assessment and critical thinking 

While we have seen above the climate risks and vulnerabilities facing the digital sector, there are other strategic issues and risks to consider. The environmental challenges of digitalisation cannot be taken in isolation as they affect all human activities (material supply, international and sovereign law, security, etc.). Thus, a relevant environmental strategy for digitalisation must also define how it interacts with national and international strategies.

## The strategic challenges of digitisation from a European perspective

It is important to situate the strategic thinking exercise from a defined area and not from an abstract or universal position. Strategic issues are issues of power, they must be situated in specific areas of power. In our case, we will start from the European position (EU27) because that is where we are. 

The first thing to note is that the European Union is not a great digital power. Not that it is home to very large digital players, but it is vulnerable in both its hardware and software foundations. The United States is a service powerhouse and equips most European countries with software and cloud solutions (AWS, GCP, Azure, Office, Gdoc, etc.). On the other hand, East Asia, notably China, is a hardware powerhouse, with a large share of resource extraction, refining and processing, component production and digital equipment. The European Union is therefore caught in a pincer movement and has limited power, for the time being, to influence the service and material issues of digitalization. This geopolitical vulnerability is therefore added to the environmental vulnerabilities already outlined in previous sessions.

<br>
<img src="img/S9-02.png" alt="Pincer movement" style="border: 1px solid gray;"><br>
<i>Pincer movement</i><br>


Faced with this observation, what are the strategic digital issues to which the environmental question is attached? Firstly, material supply is an issue directly linked to the environmental question since it directly addresses the extraction and transport of resources, a key component of the environmental analysis. Secondly, the issue of legal sovereignty is also crucial: how can environmental standards be imposed or unsustainable industrial policies be regulated if the companies in question are not subject to the law of the country in which they operate? For example, it would be complicated to force Apple, Huawei or others to extend their equipment warranty or reparability standards without strong legislative power or strong European competitors. This is directly linked to the issues of regulation and data protection, but also to the openness and transparency of data. Environmental analysis has always suffered from a lack of transparency from industry and a lack of data. Stronger legal powers or the reintegration of European industries would potentially open up these black boxes. The issue of cybersecurity is linked to our ability to defend our data and not to place them in software solutions outside the European sphere of power but also to be in control of our ecosystem. If the environmental issue invites us to limit the number of connected devices and the multiplication of software solutions, it also allows us to reduce our attack surface. Finally, the question is whether digitalization is appropriate for all citizens in a given area and whether it does not distance them from their rights and from state and business services.

<br>
<img src="img/S9-03.png" alt="Strategic digital issues" style="border: 1px solid gray;"><br>
<i>Strategic digital issues</i><br>


If we consider the digitization of human activities as a risk, then the central question of the analysis is to know how far to digitize. Indeed, today we are in a phase of intensive digitization of activities, so it seems complicated not to digitize. However, we can think about a maximum threshold of digitization according to the studied activities and the related risks.

This session does not allow us to address the links between environmental issues and every other strategic issue, so we will focus only on the material and software supply issues.

## Material supply

Material supply is key to most industries and related national economies. While the term addresses all types of resources, in the context of the digital sector, it primarily refers to the supply of minerals and metals. The U.S. and Europe rely heavily on mineral supplies from Asia, particularly China. These supply chains will be complex to rebuild elsewhere in the short and medium term, so getting out of this dependency involves long-term planning. Also, a monopoly position allows to largely influence the supply and demand mechanisms. Thus, strong economic zones such as the United States and the European Union regularly evaluate their dependence on critical raw materials, i.e. materials that play a key role in their economic and industrial activities.

<br>
<img src="img/S9-01.png" alt="US Net reliance on mineral ressources - Credits: USGS" style="border: 1px solid gray;"><br>
<i>US Net reliance on mineral ressources - Credits: USGS</i><br>


The US and the EU generally share the same suppliers of small and rare metals. They have sufficient production capacity for only a few metals (Berrylium for the US; Hafnium, Strontium, Indium and Gallium for the EU). The Asian and especially the Chinese monopoly on mineral resources is today indisputable. Africa also has minerals in great demand, such as cobalt in the DRC and small metals in South Africa. This continent is the object of a power struggle between the different economic blocks to secure their supply from Africa.

<br>
<img src="img/S9-04.png" alt="Largest share of EU supply of CRMs - Credits: European Commission" style="border: 1px solid gray;"><br>
<i>Largest share of EU supply of CRMs - Credits: European Commission</i><br>

<br>
<img src="img/S9-05.png" alt="Suppliers of Critical Raw Materials for ICT in EUs - Credits: European Commission" style="border: 1px solid gray;"><br>
<i>Suppliers of Critical Raw Materials for ICT in EUs - Credits: European Commission</i><br>


In addition to the flow of raw materials, there is also the question of manufacturing areas. Here again, the East Asian zone contains a significant part of the world's production capacity. The US government has conducted an audit of its critical supply chains for its ICT sector and has concluded that they are dependent on Chinese manufacturing lines for Printed Circuit Boards (PCBs), displays and for the assembly phase of electronic components. This finding is partly the result of a significant shift in manufacturing activities from the US, but also from Europe.

<br>
<img src="img/S9-06.png" alt="PCB manufacturing - Credits: Eolane" style="border: 1px solid gray;"><br>
<i>PCB manufacturing - Credits: Eolane</i><br>

<br>
<img src="img/S9-07.png" alt="Semiconductor production - Credits: McKinsey" style="border: 1px solid gray;"><br>
<i>Semiconductor production - Credits: McKinsey</i><br>


This brief overview of the dependencies on material chains shows in part their fragility. Geopolitical (trade war, localized conflict), economic (financial crisis, etc.), environmental (climate change, extreme weather events, etc.) circumstances can cause a series of shocks to the organization of manufacturing and logistical transport of minerals and digital equipment. For example, a conflict in Taiwan would block the production of advanced integrated circuits that are part of consumer equipment (iPhones, MacBooks, etc.) and dedicated equipment (research, military, finance, etc.). The semiconductor crisis also reminds us that the digitization of industrial production chains (cars, etc.) are particularly vulnerable. This crisis had major impacts and significantly blocked the production of cars at many car manufacturers.


<br>
![Cars](img/S9-11.jpg)<br>

In any case, we should expect strong risks of disruptions in the global mineral supply chains. These disruptions can give rise to a series of shocks that could prevent the global chains from stabilizing. It remains to be seen whether supply chains in constant crisis are the new normal or whether this will just be a one-off crisis period that can be overcome.


> Global supply chain disruptions are anticipated to become increasingly frequent and severe in the future, particularly given the looming threat of climate change. Thus, long-term supply chain sustainability and resilience will require reduced consumption of high-risk materials, achieved through lower-impact production infrastructure, material substitution, and ultimately, widespread material circularity through product reuse and recycling. However, creating a secondary supply chain will not be solved by technology alone. Achieving the scale of recycling required to meet electronics material demands will require systems-level changes through effective policies, collection systems, and economic incentives. (Shahana Althaf and Callie W. Babbitta 2020)

## Service/software supply

The challenges of digital services reveal another aspect of the potential risks of digitization. In the European case, American services have become critical in a large part of the economic area. It is considered that 80% of CAC40 companies are equipped with American cloud solutions. The French government is also a major customer of American solutions. 

> In France, Microsoft became the main software supplier to the French government in the 1990s with the adoption of Windows 95 on all government computers. Among the most dependent administrations, the Ministry of Defense and the Ministry of National Education, which again signed a contract in September 2020 to equip 800,000 workstations. The company remains the leading supplier of software to the State, encouraged by the Union of Public Purchasing Groups (UGAP), which offers Microsoft and Oracle software as a priority in its catalog. However, since 1999 many criticisms are regularly launched against the company that allows the possibility, by tracking the unique identifier of Windows, to follow the activity of a user. In addition, in 2013, Microsoft's participation in the PRISM surveillance project of the National Security Agency (NSA) started in 2007 was made public. This has not discouraged our decision makers from pursuing technological choices in favor of this company. (Ophélie Coehlo, Quand le décideur européen joue le jeu des Big techs… 2021)

<br>
<img src="img/S9-08.jpg" alt="Ranking of cloud leaders in European countries, based on revenue in Q1 2020 - Credits: Synergy Group" style="border: 1px solid gray;"><br>
<i>Ranking of cloud leaders in European countries, based on revenue in Q1 2020 - Credits: Synergy Group</i><br>

France generally makes poor geostrategic choices for digital services. The French state continues to select American cloud services for critical services (Health Data Hub, state loans managed by the BPI, etc.). The German Interior Ministry has also identified Microsoft products as a critical digital dependency for German digital services. These choices are crucial on many points: it is a question of selecting it from players in a monopoly situation and protected by American law. Hosting your data on American solutions also creates significant risks of opening the data to American authorities and of industrial or state espionage. Critical data, such as the health data of French citizens, can potentially be recovered by these actors, maliciously or not. It is a non-compliance with European law (GDPR) which forces a choice between American law or European law, the two not being compatible. American players can change their prices, their conditions of access to their service in the face of new economic or geopolitical situations.

<br>
<img src="img/S9-09.jpg" alt="The ten companies investing the most in lobbying at the European Commission - Credits: Institut Rousseau" style="border: 1px solid gray;"><br>
<i>The ten companies investing the most in lobbying at the European Commission - Credits: Institut Rousseau</i><br>


Similarly, US companies can provide software layers to ensure the operation of telecommunications equipment. Orange relies on Google technologies to develop Edge solutions. Note that the Americans do not have a telecom equipment supplier for 5G and relies on Ericsson and Nokia. 

The choices of digital services and software therefore simultaneously raise questions of legal sovereignty (in the face of American extra-territorial law and the CLOUD act) and of regulation, data protection of European citizens and companies, cybersecurity and espionage and finally openness and transparency of data. For the moment, European countries are making bad choices because the American technological giants have a service offer with a very advantageous quality / price ratio (linked to the immense subsidies from the American State (DARPA, etc.)), financial means considerable (R&D, lobbying) and are protected by US law. The European Union will take a considerable time to escape this influence if it wishes, but this will require protecting European technological players so that equivalent levels of service appear. This is the objective of a medium and long term industrial policy.

> A review conducted by the Hesse Commissioner for Data Protection and Freedom of Information (HBDI) concluded that Microsoft Office 365 and Windows 10 are not suitable for use within the German state’s schools. A principal concern is that Microsoft stores its data in a European cloud which is penetrable by the US authorities (HBDI 2019)

However, European countries are beginning to reject US services based on non-compliance with GDPR. The CNIL, the data regulator, has banned, with its European counterparts, the use of google analytics. Other countries, such as Austria, Germany or Denmark, ban Microsoft and Google services from schools. The European Union therefore has the strength to reject American services and the prospect of a Buy European Act could also favor European digital services. Such an action could reduce our dependence on American digital giants and reduce the risks associated with them.

> Denmark is effectively banning Google’s services in schools, after officials in the municipality of Helsingør were last year ordered to carry out a risk assessment around the processing of personal data by Google. (Techcrunch 2022)



## Creating synergies between strategic issues

Beyond the environmental risks, the European position is very vulnerable and exposed to many risks. Thus, digitizing more and more the economic and daily activities of this area must be closely examined. The general risk is that we will not be able to maintain the hardware and software layers in satisfactory conditions for long enough. The disruption of supply of a family of components or the loss of access to a digital service can have devastating consequences for companies and citizens alike. Whatever the technological solutions envisaged, this risk remains inherent in the structure of the digital ecosystem, but these risks can however be reduced by promoting a virtuous circle of good practices (GDPR, cybersecurity, legal sovereignty, etc.). The fact is that all these issues respond to and complement each other (sometimes oppose each other) and taken together, form a more robust digital strategy. The main angle of this strategy is to say that digitization is getting out of its safe space and a maximum digitization threshold must be defined, otherwise it will produce adverse effects on the public and private activities of the European Union. As such, environmental issues have a ready-made place and allow mutual reinforcement of these strategic issues.

<br>
<img src="img/S9-10.png" alt="Draft of synergies between strategic digital issues" style="border: 1px solid gray;"><br>
<i>Draft of synergies between strategic digital issues</i><br>


## From corporate futures to environmental realism

Digitalization has taken a central place in our lives and in the way we imagine the future. It seems difficult today to imagine forms of organization of our societies and economic activities without placing the digitization process on a pedestal while other critical infrastructures seem invisible or perceived as not very innovative (energy production, transportation, water management, etc.). This shortcut taken by our imaginations is the result of sustained work by technology companies, followed by public institutions, to reduce alternative concepts of digitization, or the viability of less digitized lifestyles. These narratives therefore give the illusion that there are no possible alternatives.


<br>
<img src="img/S10-01.jpg" alt="Gollum and Frodo purchasing an NFT of the ring - Credits: Adam Sacks" style="border: 1px solid gray;"><br>
<i>Gollum and Frodo purchasing an NFT of the ring - Credits: Adam Sacks</i><br>


However, these corporate futures have not been formulated taking into account the physical and environmental world in which they will have to evolve, far from any immateriality. Rather, the environmental perspective suggests an increasingly unstable world where resource extraction and industrial operation will become more and more risky and uncertain. There are likely to be shocks to the material supply on which the digital sector relies, both in terms of minerals and components, and equipment. In addition to the environmental dimension, there are geopolitical risks, legal tensions between different economic zones, cybersecurity risks, etc. These vulnerabilities could create a cocktail effect that would destabilize our ability to produce, import and use digital equipment and services. In this sense, the corporate futures appear obsolete. The problem is that the digitalization process increasingly links the fate of our societies to the proper functioning of the digital layer. 

From an environmental perspective, there is still a great unknown about the ability of digitalization to produce and support solutions that would mitigate and adapt to the global environmental crisis. At least we can observe that since the beginning of consumer computing, GHG emissions, material and water footprints have continued to increase without any change in trend. To imagine that digitalization has not yet produced the desired enablement effects because the world's economies are not digitized enough and the adoption rate of this or that technology is not high enough only postpones the problem. Although digitalization allows for the substitution or reduction of physical flows and equipment, it also contributes to a technological stacking. Thus, the final balance is far from clear at the macroscopic level and we know that the success or failure of a particular technology is also strongly influenced by political, cultural and economic factors, far from any purely technical consideration.

<br>
![Cow VR](img/S10-02.png)<br>
<i>A cow with a VR headset</i><br>

## An agnostic posture

Faced with this observation, it is preferable not to assume that digitalization, by default, does not support ecological transition. In fact, the actual position is counterproductive for a fundamental reason: if we imagine that digitalization always has a positive impact on the climate, then we no longer need to know where it works. It is therefore a posture of denial of evidence, and it leads to a dead end. An agnostic posture seems much more appropriate: it is then a question of understanding how and where digitalization works and where it does not. This posture invites investigation and a critical and contextual analysis of digitalization. It is a conceptual reversal compared to the ideology of immateriality of the 90s. It is a question here of saying that a digital system or service is not a sheet which would universally cover the surface of the globe, erasing its asperities. But each digital system must be analyzed case by case in each context of use with its political, cultural, social, economic components, etc. This supposes the end of the universality of the digital sector, which is all the more paradoxical at a time when we have to face problems on a global scale.

Each digital system integration must be understood in a given context as well as how this system will interact with human activities on multiple levels and through multiple factors. However, there are methodological precautions: a solution that is bad at the start will rarely be made better by a layer of digitalization – an SUV as a means of individual or collective transport remains a bad answer, whether it is autonomous/connected or not. Similarly, just because a digital service worked once somewhere doesn't mean it will work everywhere else, and the reverse is true. Understanding the effects of digitization means taking nothing at face value, especially since accessible, good-quality data is still rare.

## Future liabilities and vulnerability propagation

In a broader context, we have seen that in an increasingly uncertain world, digitizing human activities represents a certain risk. Initially, understanding and using digital tools and services is by no means innate and there is no reason to believe that all the citizens of a country will achieve a sufficient level of digital literacy. There are many reasons for having difficulties with learning digital tools: socio-economic conditions, differences in digital cultures, geographical distance, complexity of procedures, etc. If we consider that the whole of the population will never have the sufficient level to use these tools and thus assert their rights, then digitizing can represent a denial of citizenship. In this sense, it is the relationship to the State that is partly played out during the digitalization of public services.

If the material base on which the digital sector rests is crumbling because of its growing vulnerability, then it would be time to rethink our digitalization policy. Today, a digital service based on American software and hardware from minerals and components processed and assembled in Asia, could represent a liability. Tomorrow it will be the whole of digitized society which will be vulnerable due to the challenges mentioned above but also because of the risks of connection breakdown linked to extreme climatic events, and infrastructures unsuited to the new climate situation. Would a viable strategy be to stabilize the digital layer of our societies and to set the maximum threshold for the digitalization of our activities?


## Looking forward

In the current ideological context, to assume that digitalization is not the answer to all our problems seems like heresy, so limiting its progress borders on madness. However, a realistic view of the environmental and geopolitical issues rather suggests that we are heading in a possibly untenable direction, and that maintaining this direction will have an increasingly high human, political and ecological cost. An agnostic stance and a strong critical mind can initiate the slow work of rebalacing the digital sector towards long-term sustainability.





## Resources

### Syllabus
* U.S. Department of Homeland Security, [Assessment of the Critical Supply Chains Supporting the U.S. Information and Communications Technology Industry](https://www.dhs.gov/sites/default/files/2022-02/ICT%20Supply%20Chain%20Report_2.pdf), 2022


### To go further
* Shahana Althaf, Callie W. Babbitt, [Disruption risks to material supply chains in the electronics sector](https://www.sciencedirect.com/science/article/abs/pii/S0921344920305632), 2020
* European Commission, [Critical Raw Materials for Strategic Technologies and Sectors in the EU - A Foresight Study](https://ec.europa.eu/docsroom/documents/42882), 2020
* European Commission, [Critical Raw Materials Resilience: Charting a Path towards greater Security and Sustainability](https://ec.europa.eu/docsroom/documents/42849), 2020
* McKinsey, [Risk, resilience, and rebalancing in global value chains](https://www.mckinsey.com/~/media/McKinsey/Business%20Functions/Operations/Our%20Insights/Risk%20resilience%20and%20rebalancing%20in%20global%20value%20chains/Risk-resilience-and-rebalancing-in-global-value-chains-full-report-vH.pdf), 2020
