# Session 6 - The climate risks on ICT


## Defining climate risks

As we move deeper into the environmental crisis, what was once a risk becomes new living conditions. GHG emissions continue to rise, biodiversity continues to decline, we are overshooting new planetary limits and climate events are becoming more intense. It is not really a question of talking about risks, which would imply that it is avoidable, but of vulnerability, because these risks have become real and can no longer be avoided. At best we can mitigate them while we drastically change our lifestyles and production systems.


<br>
<img src="img/S6-06.png" alt="Evolution of global GHG emissions - Credits: IPCC" style="border: 1px solid gray;"><br>
<i>Evolution of global GHG emissions - Credits: IPCC</i><br>

Risk here is defined as : "Risk is defined as the potential for adverse consequences for human or ecological systems, recognising the diversity of values and objectives associated with such systems."

> In the context of climate change, risk can arise from the dynamic interactions among climate-related hazards (see Working Group I), the exposure and vulnerability of affected human and ecological systems. The risk that can be introduced by human responses to climate change is a new aspect considered in the risk concept. This report identifies 127 key risks (IPCC AR6 WRII - Summary for policymakers)

In the face of increasing risk, the less we adapt and prepare for new situations the more vulnerable we become. Because we are more vulnerable, the impacts of extreme climate events are exacerbated and have more severe consequences. The latest IPCC report also highlights the concept of maladaptation, which consists of favouring a solution that is effective in the short term but which can make the situation worse in the medium and long term. Individual air conditioning in cities is a good example of maladaptation.

<br>
<img src="img/S6-07.png" alt="The Risk model: hazards, expose, vulnerabilites - Credits: IPCC" style="border: 1px solid gray;"><br>
<i>The Risk model: hazards, expose, vulnerabilites - Credits: IPCC</i><br>

The risks identified are primarily related to climate change: droughts, fires, heat waves, floods, landslides. Secondly, these events have a profound effect on biological systems (biodiversity, etc.) and throw them out of balance, which can lead to a domino effect with unpredictable consequences. These risks therefore affect our ability to have access to water, to our habitats, to food or even to have functional infrastructures. All regions and populations are affected differently, depending on their location, economic means and capacities for adaptation and mitigation.

<br>
<img src="img/S6-09.png" alt="Key riskss - Credits: IPCC" style="border: 1px solid gray;"><br>
<i>Key risks - Credits: IPCC</i><br>

#### Activity: Explore (5min)
	Go on Aqueduct Water Risk Atlas and browse through the different water risks.
[Link to the Atlas](https://www.wri.org/applications/aqueduct/water-risk-atlas/#/?advanced=false&basemap=hydro&indicator=w_awr_def_tot_cat&lat=-14.445396942837744&lng=-142.85354599620152&mapMode=view&month=1&opacity=0.5&ponderation=DEF&predefined=false&projection=absolute&scenario=optimistic&scope=baseline&timeScale=annual&year=baseline&zoom=2)

## Climate risks on material conditions

The environmental crisis will affect the extraction of raw materials and the supply chains they feed. In the case of the digital sector, it is necessary to look closely at mineral materials. However, the electronics industry and equipment manufacturers rely on very complex supply chains including many raw materials that are difficult to track globally.

The mining sector is particularly vulnerable to access to water and energy. Water stress is therefore an important risk factor as it directly affects the processing of minerals. McKinsey publised a [study](https://www.mckinsey.com/business-functions/sustainability/our-insights/climate-risk-and-decarbonization-what-every-mining-ceo-needs-to-know) pointing out at risks linked to water stress and flooding in the mining sector. It shows that many mining sites are located in areas where water stress will increase significantly by 2040. Generally, the scientific literature on climate risks in the mining sector is very fragmented and it is complex to get a comprehensive view of the problem. Global estimates must always be complemented by much more precise and contextualised field surveys. 

<br>
<img src="img/S6-10.png" alt="Water stress and mining sites - Credits: McKinsey" style="border: 1px solid gray;"><br>
<i>Water stress and mining sites - Credits: McKinsey</i><br>


Increased demand for minerals and metals combined with increasingly difficult extraction conditions, including access to water, is likely to create disruptions in the mining industry, in addition to all the other factors discussed in Session 4. 

## Climate risks for ICT infrastructures and interdependencies

Climate risks affect human infrastructure, whether for water, transport, energy or telecommunications. In the case of the digital sector, infrastructures such as data centres or telecom networks are entirely dependent on energy infrastructures for their electricity supply. The governmental think-tank France Stratégie did a fist assessment of climate risks on French critical infrastructures. They point up several extreme weather events that can damage or affect them: heatwaves, fires, droughts, floods, land slides, strong winds and storms. They also highlight the heavy dependence of the telecom infrastructure on the electricity transmission and distribution infrastructure.

<br>
<img src="img/S6-01.png" alt="Qualification of climate risks on infrastructures - Credits: France Stratégie" style="border: 1px solid gray;"><br>
<i>Qualification of climate risks on infrastructures - Credits: France Stratégie</i><br>


> In addition to the vulnerability of the networks studied to the effects of climate change, both now and, even more so, in the face of future climate, the study highlights the great heterogeneity of the capacity to take these risks into account by the various actors. The study points out the great heterogeneity of the capacities of all stakeholders to take these risks into account. In particular, the interdependencies between these networks remain poorly addressed. (France Stratégie, Risques climatiques, réseaux
et interdépendances : le temps d’agir)

<br>
![Antenna](img/S6-02.jpg)<br>
<i>Antenna blown off by a storm, Brittany, France (2022)</i><br>

> The use of ICT for data transfer, remote control of other systems, and clock synchronisation. Pant et al. (2016) show that ICT is crucial for the successful operation of the UK’s rail infrastructure. The study shows that flooding of the ICT assets in the1-in-200 year floodplain would disrupt 46% of passenger journeys across the whole network. (Pant et al. 2016)

<br>
![Posts](img/S6-03.jpg)<br>
<i>Posts torn down by a storm in Louisiana, USA (2021)</i><br>

Beyond extreme weather events, there are trend effects such as global warming and rising sea levels that present further adaptation challenges. In a 2018 study, Durairajan estimates that 4,067 miles of fibre conduit in the US will be underwater and 1,101 nodes (e.g. points of presence and colocation centres) will be surrounded by water in the next 15 years.

> Future deployments of Internet infrastructure (including colocation and data centers, conduits, cell towers, etc.) will need to consider the impact of of climate change. Flexi- ble decision support capabilities that include risk-/failure- awareness along with other ISP objectives (e.g., revenue, growth, operational cost control, etc.) will be important in the planning process. These plans must include consideration of issues including new rights of way, costs and projections of how populations will move. (Durairajan et al., Lights Out: Climate Change Risk to Internet Infrastructure, p. 6)

<br>
<img src="img/S6-05.png" alt="Overleap of Internet Infrastructure and seawater in New York and Miami - Credits: Durairajan et al. 2018" style="border: 1px solid gray;"><br>
<i>Overleap of Internet Infrastructure and seawater in New York and Miami - Credits: Durairajan et al. 2018</i><br>

Depending on their resource requirements, some digital infrastructures may be affected differently by climate risks. For example, giant data centres in the western part of the United States use cold water cooling systems that make them more vulnerable to droughts and water shortages. We saw in a previous session that many US data centres are located in areas of high water stress, so their vulnerability to the Earth's average temperature rise is higher.

<br>
<img src="img/S3-02.png" alt="Location of data centers (white dots) and level of droughts (Western USA, May 2021)" style="border: 1px solid gray;"><br>
<i>Location of data centers (white dots) and level of droughts (Western USA, May 2021)</i><br>


In highly digitalised societies, momentary or prolonged losses of digital infrastructure are becoming increasingly disastrous as they block even basic activities. For example, during the July 2021 floods in Zhengzhou (China), at least 61,900 telecom base stations were damaged by the heavy rains, but most of them were quickly restored to service in the province. In total, at least 3,359 optical cables were damaged, with a length of 3,500 km.  As a result, there was a one-week service disruption, making routine interactions impossible: online payments via Alipay or WeChat were no longer possible, so only people with cash were able to buy food. Most banks were no longer accessible due to the disruption of the electricity grid. 

<br>
![Supermarket](img/S6-04.jpg)<br>
<i>People queuing with cash in a supermarket in Zhengzhou after telecom infrastructure flooded, China (2021)</i><br>

A similar case occurred a few weeks later in Louisiana. During Hurricane Ida, the power and telecom infrastructure were severely damaged causing a breakdown in mobile communications. More than 1,400 base stations, more than half of all base stations in the state, failed after the hurricane hit. In France, after the mudslides caused by storm Alex in the Roya valley, fixed lines were washed away by the floods. Similarly, Orange could no longer operate around forty antennas and around thirty for SFR and Bouygues. Generators were deployed to provide electricity to sites that were not damaged but cut off from the power grid and satellite phones were made available until repairs were completed.


## Adapting to risks

Identifying the risks is not a foregone conclusion, but rather a framework for defining adaptation policies to reduce our vulnerability and exposure. It is a call to action. However, we need to be sure that the responses and solutions we deploy are heading in the right direction and enable us to reduce the risk in the long term and not just in the short term.

<br>
<img src="img/S6-12.png" alt="Responding to risks - Credits: IPCC" style="border: 1px solid gray;"><br>
<i>Responding to risks - Credits: IPCC</i><br>

Adapting to risks in order to reduce them is a global strategy for adapting public policies, infrastructures and economic sectors. As such, adapting to risk is a change of culture and therefore a change of society. It also requires a shared awareness of risk factors. 

<br>
<img src="img/S6-13.png" alt="Ecosystem health influences prospects for climate resilient development - Credits: IPCC" style="border: 1px solid gray;"><br>
<i>Ecosystem health influences prospects for climate resilient development - Credits: IPCC</i><br>


## Climate vulnerabilities

The effects of the environmental crisis are hard to predict, but we know that the vulnerability of our modes of production and infrastructure has greatly increased. Resource extraction industries, generally devastating to local ecosystems, will be hit with new problems of water and energy supply. Similarly, telecom and energy infrastructures have not been designed for this new climate regime. Between the increase in extreme weather events and the trends of rising temperatures and water levels, it is almost certain that we are only at the beginning of a long crisis of infrastructure vulnerabilities. Thus, we should not assume that connectivity can always be maintained in all conditions. Similarly, designing activities that only work with an internet connection only amplifies the potential damage of a connection failure.















## Resources

### Syllabus
* IPCC, [IPCC - Climate change 2022 - Impacts, Adaptation and Vulnerability (Summary for Policymakers)](https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_SummaryForPolicymakers.pdf), 2022, pp.3-5


### To go further
* Simon Meißner, [The Impact of Metal Mining on Global Water Stress and Regional Carrying Capacities—A GIS-Based Water Impact Assessment](https://www.mdpi.com/2079-9276/10/12/120), 2021
* Ramakrishnan Durairajan et al., [Lights Out: Climate Change Risk to Internet Infrastructure](https://ix.cs.uoregon.edu/~ram/papers/ANRW-2018.pdf), <i>ANRW ’18</i>, 2018
* France Stratégie, [Climate risks, networks and interdependencies: time to act](https://www.strategie.gouv.fr/english-articles/climate-risks-networks-and-interdependencies-time-act), 2022 [FR]
* Nat Rubio-Licht, [Google and Oracle data centers are melting in the UK heat wave](https://www.protocol.com/bulletins/google-oracle-cloud-uk-heat), <i>Protocol</i>, July 19, 2022
* JRC, [Climate Vulnerability of the Supply-Chain: Literature and Methodological review](https://publications.jrc.ec.europa.eu/repository/handle/JRC93420), 2014