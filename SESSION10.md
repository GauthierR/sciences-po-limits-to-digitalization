# Session 10 - Fostering critical thinking

## From corporate futures to environmental realism

Digitalization has taken a central place in our lives and in the way we imagine the future. It seems difficult today to imagine forms of organization of our societies and economic activities without placing the digitization process on a pedestal while other critical infrastructures seem invisible or perceived as not very innovative (energy production, transportation, water management, etc.). This shortcut taken by our imaginations is the result of sustained work by technology companies, followed by public institutions, to reduce alternative concepts of digitization, or the viability of less digitized lifestyles. These narratives therefore give the illusion that there are no possible alternatives.


<br>
<img src="img/S10-01.jpg" alt="Gollum and Frodo purchasing an NFT of the ring - Credits: Adam Sacks" style="border: 1px solid gray;"><br>
<i>Gollum and Frodo purchasing an NFT of the ring - Credits: Adam Sacks</i><br>


However, these corporate futures have not been formulated taking into account the physical and environmental world in which they will have to evolve, far from any immateriality. Rather, the environmental perspective suggests an increasingly unstable world where resource extraction and industrial operation will become more and more risky and uncertain. There are likely to be shocks to the material supply on which the digital sector relies, both in terms of minerals and components, and equipment. In addition to the environmental dimension, there are geopolitical risks, legal tensions between different economic zones, cybersecurity risks, etc. These vulnerabilities could create a cocktail effect that would destabilize our ability to produce, import and use digital equipment and services. In this sense, the corporate futures appear obsolete. The problem is that the digitalization process increasingly links the fate of our societies to the proper functioning of the digital layer. 

From an environmental perspective, there is still a great unknown about the ability of digitalization to produce and support solutions that would mitigate and adapt to the global environmental crisis. At least we can observe that since the beginning of consumer computing, GHG emissions, material and water footprints have continued to increase without any change in trend. To imagine that digitalization has not yet produced the desired enablement effects because the world's economies are not digitized enough and the adoption rate of this or that technology is not high enough only postpones the problem. Although digitalization allows for the substitution or reduction of physical flows and equipment, it also contributes to a technological stacking. Thus, the final balance is far from clear at the macroscopic level and we know that the success or failure of a particular technology is also strongly influenced by political, cultural and economic factors, far from any purely technical consideration.

<br>
![Cow VR](img/S10-02.png)<br>
<i>A cow with a VR headset</i><br>

## An agnostic posture

Faced with this observation, it is preferable not to assume that digitalization, by default, does not support ecological transition. In fact, the actual position is counterproductive for a fundamental reason: if we imagine that digitalization always has a positive impact on the climate, then we no longer need to know where it works. It is therefore a posture of denial of evidence, and it leads to a dead end. An agnostic posture seems much more appropriate: it is then a question of understanding how and where digitalization works and where it does not. This posture invites investigation and a critical and contextual analysis of digitalization. It is a conceptual reversal compared to the ideology of immateriality of the 90s. It is a question here of saying that a digital system or service is not a sheet which would universally cover the surface of the globe, erasing its asperities. But each digital system must be analyzed case by case in each context of use with its political, cultural, social, economic components, etc. This supposes the end of the universality of the digital sector, which is all the more paradoxical at a time when we have to face problems on a global scale.

Each digital system integration must be understood in a given context as well as how this system will interact with human activities on multiple levels and through multiple factors. However, there are methodological precautions: a solution that is bad at the start will rarely be made better by a layer of digitalization – an SUV as a means of individual or collective transport remains a bad answer, whether it is autonomous/connected or not. Similarly, just because a digital service worked once somewhere doesn't mean it will work everywhere else, and the reverse is true. Understanding the effects of digitization means taking nothing at face value, especially since accessible, good-quality data is still rare.

## Future liabilities and vulnerability propagation

In a broader context, we have seen that in an increasingly uncertain world, digitizing human activities represents a certain risk. Initially, understanding and using digital tools and services is by no means innate and there is no reason to believe that all the citizens of a country will achieve a sufficient level of digital literacy. There are many reasons for having difficulties with learning digital tools: socio-economic conditions, differences in digital cultures, geographical distance, complexity of procedures, etc. If we consider that the whole of the population will never have the sufficient level to use these tools and thus assert their rights, then digitizing can represent a denial of citizenship. In this sense, it is the relationship to the State that is partly played out during the digitalization of public services.

If the material base on which the digital sector rests is crumbling because of its growing vulnerability, then it would be time to rethink our digitalization policy. Today, a digital service based on American software and hardware from minerals and components processed and assembled in Asia, could represent a liability. Tomorrow it will be the whole of digitized society which will be vulnerable due to the challenges mentioned above but also because of the risks of connection breakdown linked to extreme climatic events, and infrastructures unsuited to the new climate situation. Would a viable strategy be to stabilize the digital layer of our societies and to set the maximum threshold for the digitalization of our activities?


## Looking forward

In the current ideological context, to assume that digitalization is not the answer to all our problems seems like heresy, so limiting its progress borders on madness. However, a realistic view of the environmental and geopolitical issues rather suggests that we are heading in a possibly untenable direction, and that maintaining this direction will have an increasingly high human, political and ecological cost. An agnostic stance and a strong critical mind can initiate the slow work of rebalacing the digital sector towards long-term sustainability.






