# Limits to digitalization
#### The uncertain futures of digital systems in an era of environmental crisis

>This website gathers all the teaching materials and resources for the students attending the class led by Gauthier Roussilhe for [Sciences Po Paris, École d'Affaires Publiques](https://www.sciencespo.fr/public/fr.html) and for the [Master Governing Ecological Transitions in European Cities](https://www.sciencespo.fr/ecole-urbaine/fr/governing-ecological-transitions-european-cities.html).

<br>
<p align="center">
  <img width="100%" src="/img/australia.jpeg" alt="A man hugs his iMac as he makes his way through a flooded street, a woman pushes a kayak packed with the rest of their belongings.">
</p>


## Course description

First, a general overview of the materiality of digital infrastructures will be presented, both through field observation and lectures. The aim is to provide a solid foundation of what is needed to manufacture, deploy, use and dispose of digital systems. Second, we are going to look at how we assess environmental impacts of the digital sector at multiple scales. It aims at giving students a literacy that will help them navigate complex discourses and, ultimately, improve decision-making. Third, this course will look into the possible futures for ICT in a sustainable world. Real case studies from infrastructure deployment in Europe and USA, industrial development in Asia, and French territorial assessment will be studied. This last part aims at providing students with strategic and planning capabilities regarding digitalization, or possibly <i>undigitalization</i>, in an increasingly constrained world.

>To be alive right now is to find ourselves flattened against the fact that the entire human world—our cities and infrastructure, our economy and education system, our farms and factories, our laws and politics—was built for a different planet. Our understandings of how things work—the assumptions we’ve taken for granted, the experiences we’ve acquired, the skills we’ve learned—no longer offer good guidance for the chaos unfolding around us. Discontinuity surrounds us. While this is certainly true for physical systems, it’s actually even more true for our societies as a whole. Nothing is as it was. (Alex Steffen, [Discontinuity is the job](https://alexsteffen.substack.com/p/discontinuity-is-the-job))


## Course structure

This course is divided in 12 sessions of two hours each throughout the first half of the year

## Validation mode

First, this course will be evaluated on a collective project (70%). Based on the theoretical and practical knowledge given during the course, students will make a proposal to assess the environmental impact of a given technology in a small scope (city, county, etc.), and its adequation with transition policies. Second, a short individual essay (2 pages max) will also be graded (20%). Finally, participation and engagement will count for 10% of the final grade.


## Teacher

Gauthier Roussilhe is a specialist of environmental impact of digitalization. He is a PhD candidate at [RMIT](https://www.rmit.edu.au/) and is working as a consultant and scientific expert for the French administration ([DINUM](https://ecoresponsable.numerique.gouv.fr/)), national agencies such as [ADEME](https://www.ademe.fr/) or [ARCEP](https://www.arcep.fr/), and arounds targeted [topics](https://www.greendigitalcoalition.eu/advisory-board/) for the EU Commission.

His research is about exploring digital systems that are compatible with planetary limits. He focuses on the materiality of digital infrastructures (extraction, supply chains, manufacturing, deployment, maintenance, ...), and on the assessement of environmental impacts of digitalization.

Website: [gauthierroussilhe.com](http://gauthierroussilhe.com/)<br>
Email: [gauthier.roussilhe@sciencespo.fr](gauthier.roussilhe@sciencespo.fr)


---

## Licence

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is made available under the terms of the <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution - Noncommercial - Share Alike 4.0 International License</a>. 