# Session 8 - Modelling indirect effects of digitalisation


Digital systems have profoundly changed activities in post-industrial societies. Yet, it is hard to assess if these changes have been beneficial regarding climate pledges and planetary limits. Estimating those effects on systems and activites and ways of life, is at the heart of numerous debates around the overall environmental footprint of digitalization. Do the positive effects outweigh the negative effects, and if so, how and under what conditions?

<br>
![Shanghai](img/S8-02.jpg)<br>
<i>A floating QR code in Shanghai</i><br>

## Is digitisation helping the ecological transition?

It is widely accepted that digitisation is helping the ecological transition, and this idea is shared by public institutions, the private sector and even a large proportion of citizens in many countries. Because even if the digital sector has a lower environmental footprint, we need to know whether digitisation is helping other sectors to decarbonise and adapt. This involves comparing the footprint created by the digital sector with what it avoids or removes in other sectors. We mainly look at the greenhouse gas emissions avoided in other sectors, which can be called scope 4, handprint or avoided emissions.

<br>
<img src="img/S8-11.png" alt="A selection of claims on the positive impacts of digitalisation for transition policies" style="border: 1px solid gray;"><br>
<i>A selection of claims on the positive impacts of digitalisation for transition policies</i><br>

Today, there is an effort to improve the methodology for defining avoided emissions, thanks in particular to digitisation. These include the ITU-T L.1480 standard, the European EGDC methodology (under construction) and Carbone 4's Net-Zero Initiative for IT. These documents have come to light because the way in which avoided emissions are defined has so far been very confused, leading sometimes to false claims.

Because ecological transition policies are still largely underfunded, estimating avoided emissions is now of strategic interest, as proving that one is helping to decarbonise other sectors will give access to preferential financing and attract new investment. It is therefore necessary to review the foundations of these methods and past experience. 

## Taxonomies
If we talk about the environmental footprint of a digital system as a direct effect, then we talk about the effects of that system in the activities or processes where it is deployed as an indirect effect. However, there are different types of indirect effect levels, both positive and negative. In a 2016 paper, Horner et al. summarise the existing taxonomies and propose a new one. The direct, or first order, effects are those related to the manufacture, use and end-of-life of digital systems. Indirect, or "single-service" or second-order effects are the efficiency or substitution gains related to the digital service deployed. This can also include a direct rebound effect (I consume more of a service because it is more efficient/cheaper).   Indirect, or third order, effects include indirect rebound effects (I consume less on the more efficient service, so I consume elsewhere), economy-wide effects, and systemic transformations. Horner et al. provide good examples of each of these categories with a GPS system. Today, the general taxonomy is more or less fixed but the main problem is to model these effects.

<br>
<table lang="en">
<thead>
  <tr>
    <th scope="col">Scope</th>
    <th scope="col">Effect</th>
    <th scope="col">Example (from&nbsp;Horner)</th>
    <th scope="col">Related methods</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td rowspan="3">Direct / First&nbsp;order</td>
    <td>Embodied footprint</td>
    <td>Energy, materials and resources to produce a GPS system</td>
    <td rowspan="3" style="text-align:center">Life cycle assessment</td>
  </tr>
  <tr>
    <td>Operational footprint</td>
    <td>Energy, materials and resources to operate a GPS system</td>
  </tr>
  <tr>
    <td>Disposal footprint</td>
    <td>Energy, materials and resources to dispose of a GPS system at end-of-life</td>
  </tr>
<tr>
    <td>Borderline case</td>
    <td>Induction</td>
    <td>Equipement induced by the use of GPS system (satellites, etc.) – can be also considered as direct effects or rebound effect</td>
    <td style="text-align:center">Mixed</td>
  </tr>
  <tr>
    <td rowspan="3">Indirect / Second&nbsp;order</td>
    <td>Efficiency / Optimization</td>
    <td>More efficient traffic flow due to GPS-enhanced routing</td>
    <td rowspan="2" style="text-align:center">Enablement / Avoided emissions framework</td>
  </tr>
  <tr>
    <td>Substitution</td>
    <td>Replacement of paper-based maps</td>
  </tr>
  <tr>
    <td>Direct rebound</td>
    <td>More travel due to lower cost of traffic congestion</td>
    <td rowspan="4" style="text-align:center">Rebound Effect</td>
  </tr>
  <tr>
    <td rowspan="3">Indirect / Third&nbsp;order</td>
    <td>Indirect rebound</td>
    <td>Footprint consumed during time saved by efficient travel</td>
  </tr>
  <tr>
    <td>Economy-wide rebound (Structural change)</td>
    <td>GPS enables autonomous vehicles and causes growth of intelligent transportation system manufacturing</td>
  </tr>
  <tr>
    <td>Systemic Transformation</td>
    <td>Autonomous vehicles alter patterns in where people choose to live and work</td>
  </tr>
</tbody>
</table>

  <caption><i>Sources: adapted from Horner (2016) Known unknowns: indirect energy effects of information and communication technology; Bieser & Hilty (2018) Assessing Indirect Environmental Effects of Information and Communication Technology (ICT): A Systematic Literature Review; Coroama et al. (2020) A Methodology for Assessing the Environmental Effects Induced by ICT Services – Part I: Single Services</i></caption>

## Current issues

Faced with the lack of consistent and credible methodology, Coroama et al. proposed a methodology to assess the
environmental effects induced by ICT services, from the perspective of a single service, and Bergmak et al. from the perspective of multiples services and companies. They address the main issues met so far: 
* substituting versus optimizing ICT services ; 
* the time perspective ; 
* the choice of the baseline ;
* the extrapolation method and rebound effects.

Similarly, Rasoldier et al. point out that existing literature tends to ignore:
* life-cycle impacts, i.e. the environmental footprint of the ICT service/product studied ;
* structural impacts (rebound effects, society-wide effects). 

Overall, the issues faced with the existing literature is the focus on positive effects, while ignoring or minimizing possible negative outcomes. Extrapolating case studies leads to several shortcomings pinpointed by Malmodin and Coroama: 
* the success and failure of ICT services depend of factors outside of the ICT sector (policies, price, culture, etc.) that are generally overlooked ; 
* the environmental impact of the ICT service studied is not considered ; 
* the representativeness of case studies is critical but rarely discussed, especially if extrapolated.

Court and Sorrell looked more closely at the digitization of goods to assess their influence on energy consumption and carbon emissions. Out of 31 studies based on life-cycle analysis, authors show a great variability of estimated energy savings, from -90% to +3000% for e-publications for instance, pointing to the importance of key assumptions. They conclude that a narrow scope leads to overly optimistic results as most studies assume complete substitution of material goods by digital goods, and don’t account for rebound effect. They note that studies with a larger scope don’t show large energy savings and can present an increased energy consumption.

Furthermore, the expected benefits could be negligible compared to the transformation effort needed and some solutions could interact negatively: for instance, according to Coulombel et al., improving public transit infrastructures and developing carpooling services in the same area can be counterproductive.



## Efficiency, substitution: indirect effects of a service

In many ways digital services can make activities more efficient from an industrial or managerial point of view. For example, a GPS navigation system usually allows for shorter or shorter journeys in relation to road traffic. This is particularly effective with complex road systems and high travel intensity activities such as road freight. Similarly, digital services can replace other products, services or activities. For example, video conferencing can replace a car or plane journey to a conference/seminar. A digital application such as Google Maps or OpenstreetMaps can replace a dedicated GPS that comes with its own software. Generally, if we focus on the narrow scope of the digitised activity, we can estimate efficiency and/or substitution effects. Estimates from industry are generally focused on this perimeter only.

According to the systematic literature review performed by Bieser and Hilty the most common application domains of ICT are virtual mobility, virtual goods and smart transport. 

The ITU-T L.1480 standard proposes to define the indirect effects of a digital service using a consequence tree as shown below.

<br>
<img src="img/S8-12.png" alt="Consequence tree of a e-shopping service - Credits: ITU" style="border: 1px solid gray;"><br>
<i>Consequence tree of a e-shopping service - Credits: ITU</i><br>

Estimating avoided emissions involves comparing two scenarios: a reference scenario in which the digital service is not introduced, and a scenario in which it is. The definition of the reference scenario is crucial, because if it is overestimated, then the emissions avoided will also be overestimated, and vice versa.


<br>
<img src="img/S8-13.png" alt="Historical and reference scenario VS scenario with a new solution - Credits: WBCSD" style="border: 1px solid gray;"><br>
<i>Historical and reference scenario VS scenario with a new solution - Credits: WBCSD</i><br>

> When quantifying the avoided emissions impact of their solutions, companies should follow the step-by- step approach below and ensure that the calculations rules detailed in the guidance are followed to achieve a robust and consistent approach (WBCSD).

<br>
<img src="img/S8-15.png" alt="The five steps to ensuring a consistent approach to assessing avoided emissions - Credits: WBCSD" style="border: 1px solid gray;"><br>
<i>The five steps to ensuring a consistent approach to assessing avoided emissions - Credits: WBCSD</i><br>


#### Activity: Modelling house energy management system (HEMS) (together, 10min)

    If you were to estimate the impact of smart home energy mangement
    systems for one year per household, what would be the consequence tree?




## Looking at the historical assumptions in the digital sector

Since the early 2000s, many organisations have tried to model the emissions avoided through digitisation. The reports most widely used by the private and public sectors until recently are GeSI's SMARTer 2030 and GSMA's Enablement Effect.

<br>
<img src="img/S8-14.png" alt="Claims from GeSI and GSMA reports" style="border: 1px solid gray;"><br>
<i>Claims from GeSI and GSMA reports</i><br>


### SMARTer2030 : Digitalization could reduce global GHG emissions by 20% by 2030 (GeSI, 2015)

In 2015, GeSI, an organisation representing the ICT industry on environmental issues, published a new report to estimate the emissions avoided through digitalization. They estimate that global emissions could be reduced by 20% (12 GtCO2e) by 2030 through intensive digitalization of human activities. This report focuses on efficiency and substitution effects in several sectors (energy, industry, transport, agriculture, smart cities, logistics, services) and estimates at the margin direct rebound effects without integrating them in the final result. This type of report is based on over-optimistic assumptions of efficiency with unproven adoption rates.


<br>
<img src="img/S8-03.png" alt="Reminder of the assumptions of the GeSI report" style="border: 1px solid gray;"><br>
<i>Reminder of the assumptions of the GeSI report</i><br>


These assumptions and adoption rates are then extrapolated to the global level to estimate possible avoided emissions. Given the input data and methodology, it is preferable not to use these figures and not to base policy decisions on them.


### The Enablement Effect : 1 tonne of CO2e emitted by the digital sector means 10 tonnes avoided in other sectors

In 2019, the GSMA, the trade body representing telecom operators, published a report to estimate the emissions avoided through the digital sector. Like GeSI, this report focuses largely on efficiency and substitution gains. However, the estimates are mainly focused on the mobile technology sector, to which telecom operators contribute. They conclude that one tonne of CO2e emitted through mobile technologies avoids 10 tonnes in other sectors en 2018 (2.1 GtCO2e).

<br>
<img src="img/S8-05.png" alt="GSMA's 1:10 ratio explanation" style="border: 1px solid gray;"><br>
<i>GSMA's 1:10 ratio explanation</i><br>


In fact, this ratio is obtained by comparing the carbon footprint of telecom operators with the emissions avoided by the digital sector as a whole. This ratio is therefore greatly overestimated and the estimated avoided emissions remain to be verified.

More than half of the emissions avoided in this report are related to an extrapolation of the results of a study conducted on 6,100 people regarding their smartphone use. The representativeness of the sample is questionable, especially when it comes to making a global extrapolation. The method of calculating avoided emissions also needs to be explained to understand the limitations of this kind of modelling exercise.

<br>
<img src="img/S8-06.png" alt="Example of estimate of avoided emissions from home-sharing [1/2]" style="border: 1px solid gray;"><br>
<i>Example of estimate of avoided emissions from home-sharing [1/2]</i><br>

<br>
<img src="img/S8-07.png" alt="Example of estimate of avoided emissions from home-sharing [2/2]" style="border: 1px solid gray;"><br>
<i>Example of estimate of avoided emissions from home-sharing [2/2]</i><br>


Extrapolation from data whose representativeness remains to be demonstrated, and the use of reduction hypotheses from a single industrial source renders the modelling exercise invalid. As with the GeSI report, the focus on efficiency and substitution effects at the expense of other effects, and the use of low-quality methodology, reflects the main purpose of these reports: to prove that digitalisation can avoid emissions no matter what. The aim is not to understand under what conditions digitalization can actually reduce emissions from human activities. Unfortunately, the GeSI and GSMA reports are very often mobilised in the writing of public policies and the development of positive discourses around digitalization.


## Induction, indirect, economy and society-wide: rebound effects 

Rebound effect is one of the indirect effect that is particularly hard to track as it can apply from the micro to the macro scale. A rebound effect is identified as a positive impact (less resources consumed, more efficient process) on a service or product which leads to an increased demand on the latter or beyond. A rebound effect is not necessarily negative as the rebound may have a lower impact than the solution it replaces. Therefore, induction effects link to a more neutral term, that can be both positive and negative. These two terms circulate in a similar way in the scientific literature on the subject. 

Coroama and Mattern identify several types of rebound effect that could apply to digital technologies: a direct rebound effect (optimization of a product leads to an increased consumption), a backfire (when the rebound effect is more impactful that the original solution), indirect rebounds (when the rebound happens out of the original scope), time rebound (linked to the cost of time-saving technologies), and macro-level rebounds (economy/society-wide). For instance, based on English travel patterns from 2005 to 2019, Caldarola and Sorrell estimate that while teleworkers take less trips than non-teleworkers, they travel farther, suggesting that teleworking does not reduce travel. 

> Our results provide little support for the claim that teleworking reduces travel. Indeed, after controlling for a range of variables, we find that the majority of English teleworkers travel farther each week than non-teleworkers. This results from a combination of longer commutes and additional non-work travel. There appears to be a ‘tipping point’, however. If people telework three or more times a week, their weekly private travel (commuting + non-work) is less than that of non-teleworkers. We also find that the total weekly travel of all household members is greater in households where one member is teleworking, suggesting the presence of intra-household effects that further erode the benefits of fewer commutes. (Caldarola and Sorrell 2022)

On a similar topic, Ward et al. study the effects of switching from private vehicle travel to transportation networks companies (TNCs) such as Uber or Lyft. Based on a randomized sampling (Monte-carlo) simulation of 100,000 passenger trips in six U.S. cities, they estimate that shifting from private vehicle travel to TNCs reduces air polluants by 50-60% on average, increases fuel consumption and greenhouse gases emissions by 20% due to deadheading, and external costs by 60% due to congestion, crashes and noise.

> On average, we find that shifting travel from a private vehicle to a TNC (Transport Network Companies) reduces externalities from conventional air pollution, increases externalities from GHGs, and reduces the total external costs from all air emissions combined. However, increased vehicle travel from deadheading also increases total congestion, crash, and noise costs, causing a net increase in total external costs, on average (+$0.35/trip, on average). When TNCs displace transit, walking, or biking, rather than personal vehicles, the increase in externalities is about three times larger (+$1.20/trip). However, when TNCs rides are pooled, TNCs can reduce externalities when displacing personal vehicles (−$0.60/trip) but not when displacing transit (+$0.85/ trip) (Ward et al. 2022)

At another scale, a mix of digital services can be modelled in a given city or territory. It is then a question of thinking about the modelling in terms of the precise characteristics of a territory, its geography, its population, etc. This also makes it possible to study the deployment of a given technology with more precise data and without extrapolating too much. For example, Ipsen et al. studied the environmental impact between Copenhagen and a "smart" version of the city that includes nine different technologies. The study shows a decrease in studies during the use phase but an increase in impacts during the resource extraction, manufacturing and end-of-life phases.

<br>
<img src="img/S8-09.png" alt="Copenhagen" style="border: 1px solid gray;"><br>
<i>Copenhagen smart city's study</i><br>



## Beyond digitalization

Looking only at direct effects and efficiency and substitution effects provides a very small, and usually false, picture of the real environmental impact of digital solutions. Similarly, extrapolating the use of a digital service implies extrapolating all the success factors: bike sharing applications will have much less effect in cities without bike infrastructure, connected thermostats will have less effect in buildings without good insulation. Indirect effects usually depend on factors external to the digital service. With this in mind, extrapolating the benefits or harms of digital services becomes much more complex, if not irrelevant. It is now necessary to understand the deployment and use of a technology over a sufficiently large area of use to observe the full range of indirect effects while avoiding extrapolation. Under these conditions, the positive effects of digitisation are much less significant than estimated and remain insufficient to achieve the ecological transition targets.


> [...] the mechanisms behind rebound effects in general, and thus of digital rebound as well, are essentially non-technical in nature. Their roots reside in eco- nomics and in human behavior. It is thus highly unlikely that digital rebound can be addressed solely through technologi- cal means. While digitalization does often wait on the side- line, ready to provide efficient substitutes for existing tech- nologies and processes, the avoidance of digital rebound effects needs to be enforced differently, possibly by policy measures. (Coroama et Mattern, 2019)

<br>
![Meme](img/S8-10.jpg)<br>

## Resources

### Syllabus
* Vlad Coroama and Friedemann Mattern, [Digital Rebound – Why Digitalization Will Not Redeem Us Our Environmental Sins](http://ceur-ws.org/Vol-2382/ICT4S2019_paper_31.pdf), 2019

### To go further
* Nathaniel Horner et al.,[Known unknowns: indirect energy effects of information and communication technology](https://iopscience.iop.org/article/10.1088/1748-9326/11/10/103001), 2016
* Vlad Coroama et al., [A methodology for assessing the environmental effects induced by ict services: Part i: Single service](https://arxiv.org/abs/2006.10831), 2020
* Permilla Bergmark et al., [A methodology for assessing the environmental effects induced by ict services: Part ii: Multiple services and companies](https://dl.acm.org/doi/10.1145/3401335.3401711), 2020
* C. Bieser and L. M. Hilty, [Assessing indirect environmental effects of information and communication technology: A systematic literature review](https://www.mdpi.com/2071-1050/10/8/2662), 2019
* B. Caldarola and S. Sorrell, [Do teleworkers travel less? evidence from the english national travel survey](https://www.creds.ac.uk/publications/do-teleworkers-travel-less-evidence-from-the-english-national-travel-survey/), 2022
* Nicolas Coulombel et al., [Substantial rebound effects in urban ridesharing: Simulating travel decisions in Paris, France](https://www.sciencedirect.com/science/article/abs/pii/S1361920918303201), 2019
* Aina Rasoldier et al., [How realistic are claims about the benefits of
using digital technologies for ghg emissions mitigation?](https://computingwithinlimits.org/2022/papers/limits22-final-Rasoldier.pdf), 2022
* Steve Sorrell et al., [Digitalisation of goods: a systematic review of the determinants and magnitude of the impacts on energy consumption](https://iopscience.iop.org/article/10.1088/1748-9326/ab6788), 2020
* J. W. Ward et al., [Air pollution, greenhouse gas, and traffic externality benefits and costs of shifting private vehicle travel to ridesourcing service](https://pubs.acs.org/doi/abs/10.1021/acs.est.1c01641)