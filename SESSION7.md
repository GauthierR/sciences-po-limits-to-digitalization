# Session 7 – Case study: Electronic manufacturing in Taiwan

> This session uses the research paper from Gauthier Roussilhe, Thibault Pirson, Mathieu Xhonneux and David Bol, titled: From Silicon Shield to Carbon Lock-in? The Environmental Footprint of Electronic Components Manufacturing in Taiwan (2015-2020) ([Link to preprint](https://arxiv.org/abs/2209.12523)).

## Why Taiwan?

Over the last four decades, Taiwan has developed a strong ecosystem in semiconductor manufacturing, building a leadership position in packaging, foundries, and testing of integrated circuits (ICs). The island is a key provider of electronic components for the rest of the world with more than 63% of the 2021 worldwide output value ensured by Taiwanese IC foundries and 58% of the IC packaging. The electronics industry accounts on average for 30% of Taiwan’s exports and it is therefore not surprising that it significantly contributes to the economy of the island. In fact, Taiwan’s semiconductor industry has witnessed double-digit growth for three consecutive years and is forecasted to reach more than 170 billion US dollars in 2022. The Taiwanese government heavily relies on the electronics industry for the next decades, as it plans on establishing several semiconductor initiatives and R&D programs to support its growth. In parallel, the Taiwanese government has issued a carbon reduction plan called the Greenhouse Gas Reduction Action Plan, with the objective of decreasing their absolute carbon footprint at the 2050 horizon. Yet, Taiwan has been struggling with the effective implementation of this plan and actual carbon reductions are still to be realized. As the production of electronic components is known to be energy and resource-intensive, their environmental impacts cannot be ignored.

<br>
![Wafer](img/S7-01.jpg)<br>

Western economies as both the USA and the European Union (EU) are currently massively investing to relocate the manufacturing of advanced manufacturing processes in their own territory (European Chips Act). It is clear that the EU aims at fostering a large digital innovation capacity based on a resilient semiconductor ecosystem. A key policy objective is to eventually bridge the gap from lab to fab by producing CMOS chips in European foundries in order to secure its electronics supply chain and boost its competitiveness through digitalization. This is further supported by the recent chip shortage which revealed the increasing pressure on our production systems. Analyzing the current Taiwanese situation can provide a good estimate of the environmental impacts associated with the development of current and future state-of-the-art electronic components, given that more than 90% of the worldwide sub-10nm integrated circuits (ICs) are currently manufactured in Taiwan at TSMC, the *de facto* world leader in advanced ICs manufacturing.

## Making electronic components

Before going any further it is necessary to have a basic understanding of what electronic components are and how they are made. Electronic components are generally classified into different families according to their function and physical properties. There are many ways of classifying them but there are three main categories: active components (diodes, transistors, ICs, optoelectronics, displays), passive components (resistors, capacitors, sensors, etc.) and electromechanical components (switches, fuses).

<br>
![Electronic Components](img/S7-03.png)<br>
<i>Close-up of a microcontroller (left) and side view of a microprocessor (right) - Credits: Eric Schlaepfer and Windell H. Oskay</i><br>

Within the integrated circuits there are still different families, called logic, i.e. with logic/electronic gates (diode/transistor/resistor logic). The integrated circuits that interest us are the CMOS (Complementary Metal Oxide Semiconductor) logic components used for microprocessors or memory components.

<br>
<img src="img/S7-02.jpg" alt="Manufacturing process for ICs" style="border: 1px solid gray;"><br>
<i>Manufacturing process for ICs</i><br>


Processors are etched on silica disks, called wafers, doped with another element (e.g. Boron) to modulate their electrical and structural properties. The electronic circuit patterns are affixed to masks that guide the etching, like the negative of a photograph. A wafer allows many circuits to be etched at once, and these circuits are cut with a saw. They are then packaged, i.e. the component connectors are plugged in and tested, and placed in a plastic frame to prevent damage. These components are then sent downstream to the other industries to be soldered onto a circuit board.

<br>
<img src="img/S7-11.png" alt="Simplified processes for making ICs" style="border: 1px solid gray;"><br>
<i>Simplified processes for making ICs, see high-res [here](https://gauthierroussilhe.com/en/resources/la-fabrication-de-composants-electroniques)</i><br>

The machines that allow this type of production are very expensive, especially those for engraving. Every year manufacturers produce smaller transistors and can therefore fit more on the same surface. The more transistors there are, the more computing power there is. Today, the most advanced methods engrave at 5nm (the distance separating each transistor from the other) but the physical limit is 2nm and we will reach it within a decade. Today, there is only one type of process that allows us to etch below 7nm, and that is Extreme Ultra Violet. Only one manufacturer produces machines using this process, ASML, and 90% of the production capacity below 10nm is located in Taiwan


<br>
<img src="img/S7-04.jpg" alt="NXE:3400 machine from ASML" style="border: 1px solid gray;"><br>
<i>NXE:3400 machine from ASML</i><br>


## Looking at Taiwan's electronic industry

The manufacturing of the electronic components under study implies complex supply chains with several actors located in different countries. The chain could be divided in two main parts, the front-end and the back-end processing. [...] As a first step to map this complex ecosystem, we narrow down the scope of this study by excluding back-end processing which is generally less impacting than front-end manufacturing. We further narrow the scope of this study by excluding design and raw material extraction as we focus exclusively on the environmental impacts of ECMs that are geographically located on the island of Taiwan. 


<br>
<img src="img/S7-05.png" alt="Simplified view of semiconductor manufacturing processes and the scope of the study (classification based on Taiwanese Indexes of production)" style="border: 1px solid gray;"><br>
<i>Simplified view of semiconductor manufacturing processes and the scope of the study (classification based on Taiwanese Indexes of production)</i><br>


We consider in this study three indicators that quantify the main environmental impacts of ECMs: GHG emissions, final energy consumption, and water usage.


### Results

The data shows a strong 43.3% increase of GHG emissions during the period 2015-2020 (+43.3%), which corresponds to a CAGR of +7.5%.[...] The final energy consumption of the 16 ECMs in our sample has increased by +18.4% over 3 years, corresponding to a CAGR of +8.8%.[...] The global volume increased by +34.4% over 6 years, corresponding to a CAGR of +6.1%.

<br>
<img src="img/S7-09.jpg" alt="National shares of (a) electricity consumption, (b) GHG emissions, and (c) water consumption in 2020 for the different Taiwanese sectors. The contribution of the ECMs from our sample belong to the electronics sub-sector, which itself is part of to the industrial sector" style="border: 1px solid gray;"><br>
<i>National shares of (a) electricity consumption, (b) GHG emissions, and (c) water consumption in 2020 for the different Taiwanese sectors. The contribution of the ECMs from our sample belong to the electronics sub-sector, which itself is part of to the industrial sector</i><br>


The figure below depicts the normalized evolution of the three aggregated indicators and of the number of manufactured ICs in Taiwan from 2015 to 2020, according to data from the Taiwanese Ministry of Economic Affairs.

<br>
<img src="img/S7-08.jpg" alt="Evolution of the environmental indicators obtained in this study and of the ICs manufactured in Taiwan over the period 2015-2020. All data is normalized with respect to 2016, i.e., 2016=100" style="border: 1px solid gray;"><br>
<i>Evolution of the environmental indicators obtained in this study and of the ICs manufactured in Taiwan over the period 2015-2020. All data is normalized with respect to 2016, i.e., 2016=100</i><br>


### Territorial constraints

To further study the long-term sustainability of the Taiwanese electronics industry, we now analyze the territorial
constraints of Taiwan regarding renewable electricity generation and water supply.

Up to 97.5% of Taiwan’s energy generation originates from imported carbon-intensive energy sources in 2020, including coal from Australia, Indonesia and Russia, crude oil from Saudi Arabia, Kuwait and US and liquefied natural gas (LNG) from Qatar, Australia and Russia. Regarding specifically the production of electricity, the Taiwanese government plans to phase out nuclear power and to produce 20% of its electricity from renewable sources by 2025, under the so-called 2017 Electricity Act. Yet, renewable energy represented less than 6% of the power generation in 2020. In fact, as of 2020, only 9.47 GW of renewable energy capacities have been installed in Taiwan
(including 5.82 GW from photovoltaic power and 0.85 GW from onshore and offshore wind power). Renewable energies generated that year 15.12 TWh of electricity, whereas the sole electronics sub-sector consumed 50.73 TWh. Due to the limited usable space on the island, the Taiwanese government mainly counts on offshore wind turbines in the future, with a target capacity of 5.7 GW by 2025. Yet, by June 2021, only 0.13 GW have been installed.

<br>
<img src="img/S7-06.png" alt="Location of companies considered in this study together with power plants and water reservoirs compared to population density" style="border: 1px solid gray;"><br>
<i>Location of companies considered in this study together with power plants and water reservoirs compared to population density</i><br>

<iframe width="100%" height="450" frameborder="0" title="Felt Map" src="https://felt.com/embed/map/From-Silicon-Shield-to-Carbon-Lock-in-m7R9C29C9AbQ4uXAewdv2c7XB?loc=23.63,121.123,7z"></iframe><br>

Even if ECMs do not represent a significant part of the national water intake, they rely on
a limited number of reservoirs located in places of very high population density. For instance, TSMC’s factories in Hsinchu Science Park use 10.3% of the daily supply from Baoshan and Second Baoshan reservoirs whereas those located at STCP (Southern Taiwan Science Park) use 5.3% of the daily supply from Nanhua and Zengwen reservoirs. This concentration has already led to supply issues during droughts. In 2020, the island faced a drought caused by the absence of typhoons in the previous year. If ECMs were already facing stress in the water-intensive production chain, this drought seriously amplified the crisis in this key sector of the island. Indeed, TSMC relied that year on hundreds of water trucks to maintain its water supply while several restrictions where being taken at the national level. In this sense, Taiwanese ECMs share a liability regarding water supply due to the increasing and spatially concentrated demand in the Science Parks.

### Carbon reduction roadmap

The Taiwanese roadmap draws a GHG reduction roadmap up to 2050 which motivates a long-term projection in this case. Here, we only use conservative assumptions to design these scenarios. We do not assume improvements or degradation of the efficiency of manufacturing processes, since such hypotheses strongly depend on the type of products that are considered. In addition, the scenarios use the 2020 values from our sample of 16 ECMs, and not the extrapolated value for the whole sub-sector. [...] The results point out the inconsistency between the GHG emissions from the electronics sub-sector and Taiwan’s GHG roadmap. Indeed, following the different scenarios, at least 15% to 37% of the 2050 carbon budget would be allocated to the manufacturing of electric components. These percentages should even be revised upwards when considering the entire sub-sector. In all cases, our analysis clearly highlights the difficult upcoming political arbitrations that will be needed if ECMs cannot decrease their GHG emissions.

<br>
<img src="img/S7-10.jpg" alt="Scenario analysis for ECMs’ GHG emissions in Taiwan from 2020 to 2050. Perspectives regarding the national GHG emissions and the Taiwanese carbon reduction roadmap. The share of each scenario with respect to the total carbon budget in 2050 is given on the bottom right" style="border: 1px solid gray;"><br>
<i>Scenario analysis for ECMs’ GHG emissions in Taiwan from 2020 to 2050. Perspectives regarding the national GHG emissions and the Taiwanese carbon reduction roadmap. The share of each scenario with respect to the total carbon budget in 2050 is given on the bottom right</i><br>



## Towards a Potential Carbon Lock-in

A carbon lock-in describes a situation where, in a ecoomical context of returns to scale, both public institutions and private actors (which constitute, with the deployed technological infrastructure, a techno-institutional complex) drastically inhibit the competitiveness and roll-out of low-carbon alternatives.

<br>
<img src="img/S7-07.png" alt="Illustration of the techno-institutional complex fostering a lock-in
on carbon-intensive energy sources" style="border: 1px solid gray;"><br>
<i>Illustration of the techno-institutional complex fostering a lock-in
on carbon-intensive energy sources</i><br>


We first point out that the Taiwanese geopolitics and economy are mostly centered around its electronics industry. The Taiwanese silicon shield is a well-described geopolitical strategy against the ambitions of mainland China on the island. This strategy relies on being a worldwide leading supplier of electronic components, enabling to seek the military protection of the United States. To reach this crucial leading position, the Taiwanese government fostered the electronics industry by creating the original model of Science Parks, and keeps to heavily subsidizing it. In return, leading Taiwanese ECMs have also committed substantial investments, e.g., TSMC is investing up to 44 billion USD in 2022 to increase production and R&D efforts. Yet, the technological path towards advanced sub-10nm technologies also implies new industrial processes such as EUV, which in turn are likely to increase the environmental footprint and energy consumption of the industry (the energy consumption from EUV processes is more than ten times that of deep ultraviolet (DUV) processes). At the same time, the territorial analysis previously discussed showed the difficulties for Taiwan to conduct a large-scale deployment of renewable energies in the short and medium-term. We therefore expect that a further grow of the production volume of ECMs will require increasing imports of high-carbon energy supplies. When considering these interactions together, it becomes apparent that the Taiwanese electronics industry is fated to remain a major contributor of GHG emissions.

## Lessons to be learned

The Taiwanese case is specific to an industrial/economic development and a geopolitical context. Thus, the lessons to be learned from this case study need to be carefully selected. Firstly, Taiwan reminds us that even in a carbon neutral world there will be carbon-intensive zones. Secondly, it shows the fragility of our electronic production chains, both environmentally and climatically, and geopolitically, as a war in Taiwan would block the global production of new generation chips. Finally, it also leads us to ask whether the technological development path chosen is the most appropriate for the new situation on Earth. For the moment it seems not.


## Resources

### Syllabus
* Nikkei Asia, [How Taiwan became the indispensable economy](https://asia.nikkei.com/static/vdata/infographics/taiwan-economy/)
* Gauthier Roussilhe et al., [From Silicon Shield to Carbon Lock-in? The Environmental Footprint of Electronic Components Manufacturing in Taiwan (2015-2020)](https://arxiv.org/abs/2209.12523)


### To go further
* David Bol et al., [Moore’s Law and ICT Innovation in the Anthropocene](https://ieeexplore.ieee.org/document/9474110), <i>IEEE Design, Automation & Test in Europe Conference & Exhibition (DATE)</i>, 2021
* Paul E. Ceruzzi , [Moore's Law and Technological Determinism: Reflections on the History of Technology](https://www.jstor.org/stable/40060905), <i>Technology and Culture</i> 46, n. 3, 2005
* Financial Times, [The big question of how small chips can get](https://www.ft.com/content/fbf52ede-e1a9-4797-9e52-3f80a7d855d9)
* Susannah Glickman, [Semi-Politics](https://www.phenomenalworld.org/analysis/semi-politics/)
