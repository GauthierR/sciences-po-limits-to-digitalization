# Session 2 - Understanding digital infrastructures


## What are digital infrastructures?

The transfer, computing and use of digital data require a series of interconnected infrastructures. Three types of systems are generally distinguished in environmental science applied to digitization: data centers, telecommunications networks, and user equipment. Understanding how these infrastructures work and their materiality is a prerequisite for environmental analysis.



#### Activity: Field observation (60 minutes)

	Take a copy of the field notebook provided by the teacher and start to document 
	the ICT infrastructure around the school. Come back to class after to fill the 
	online collaborative map with your pictures and annotations. The online map will
	be provided by the teacher when students are coming back to class.

Download the field notebook [here](pdf/FBN-V1.pdf) (right click, open in a new tab)

efzefzez

### Reference map

<iframe width="100%" height="450" frameborder="0" title="Felt Map" src="https://felt.com/embed/map/ICT-Infrastructures-Ug7eiyn9AQNODDW9B9B2PUuIB?loc=46.39,2.91,5z"></iframe>

### Data centers
They are different ways to store and compute data. Data centers differs in scale and services. Originally, companies would have their own server rooms in their headquarters, these rooms are called "on premise" (on prem). Telecom operators would have their own data centers that can also perform interconnection between different networks. Then, there are data center operators that offer to rent space, servers and virtual machines in their facilities, this kind of activity is called colocation. Finally, public cloud operators (Google Cloud Platform, Amazon Web Services, Alibaba, etc.) offer managed services to its clients with a consumption-based pricing, instead of renting space or machines the client only pays for what it consumes. Public cloud providers tend to develop large-scale data centers called "hyperscalers". Organizations can also run their own private cloud on their servers (on premise) or on the servers they are renting in a colocation data center.

<br>
<iframe width="100%" height="450" src="https://www.youtube-nocookie.com/embed/80aK2_iwMOs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


<br>
<img src="img/S2-01.jpg" alt="Pictures of a server rack and of a Dell server" style="border: 1px solid  gray;">
<i>Left: To give a sense of scale, these 2 cabinets are capable of handling 50% of Wikipedia’s traffic, hosted in Equinix's colocation data center in Marseille. Credits : Wikimedia / Right: Close-up of a Dell server</i><br>


### Telecommunication networks

Telecommunications networks cover a wide variety of infrastructures. We distinguish between fixed and radio access networks that connect fixed (computers, etc.) or mobile (smartphones, etc.) digital equipment. These networks send generally data to aggregation and core networks who will route the transferred data to its destination. Each telecom operator has its own autonomous network that interconnects with private networks of companies, states, and various organizations. This interconnection between autonomous networks is ensured by what is called the "internet protocol" (IP).

Fixed networks include copper networks, historically used by the telephone, and fiber networks, which are now being massively deployed in post-industrial countries. A customer connected to the fiber network has a box of his operator which is connected to the line opened by the latter. The fiber link will pass through a multitude of small equipments before arriving in the operator's data center for processing.

<br>

<img src="img/S2-03.jpg" alt="Fixed Access Networks poster" style="border: 1px solid  gray;">
<i>A simplified view of fixed accesss networks – See high-res [here](https://gauthierroussilhe.com/en/resources/fixed-access-networks)</i><br>

<br>
<img src="img/S2-07.jpg" alt="Poster of each components of an Fixed Access Network" style="border: 1px solid  gray;">

Mobile networks include 2G/3G/4G/5G stations as well as satellite solutions. A mobile device such as a smartphone maintains a permanent connection with the nearest station in order to transmit data over the operator's network and the internet. Mobile coverage depends on the number of stations deployed on the territory. Urban areas are more easily covered and can reach many users. On the other hand, rural areas are more difficult to cover due to the low financial incentives and more complex geographies.

<br>
<img src="img/S2-04.jpg" alt="A simplified view of radio access networks" style="border: 1px solid  gray;">
<i>A simplified view of radio access networks – See high-res [here](https://gauthierroussilhe.com/ressources/les-reseaux-d-acces-mobiles)</i><br>

<br>
<img src="img/S2-08.jpg" alt="Pictures of Remote accesss networks" style="border: 1px solid  gray;">

Connections coming from the fixed and mobile networks then circulate on the aggregation networks until they arrive on the core network of the operator. It is usually at this point that the incoming connection is processed and rerouted to the requested destination through internet protocol. Long-haul fiber networks cover the national territory and continue into other bordering countries.

<br>
<img src="img/S2-05.jpg" alt="A simplified view of aggregation and core networks" style="border: 1px solid  gray;">
<i>A simplified view of core networks – See high-res [here](https://gauthierroussilhe.com/en/resources/core-networks-and-beyond)</i><br>




### User equipment

The term "user equipment" or "end-user devices" refers to all equipment used by end customers, whether they are professionals or consumers. This includes desktop and laptop computers, monitors, phones, smartphones, tablets, peripherals. The "Entertainment & Media" sector also has its share of equipment: cameras, audio systems, media players, smart speakers, connected watches, game consoles, etc.

The development of the Internet of Things (IoT) poses a new challenge to the inventory of all this equipment as it will be included in all sectors: surveillance cameras, sensors, etc. It is difficult today to know the exact number of connected objects and other equipment deployed. The Internet of Things involves turning more and more everyday objects into connected objects, which makes future inventory between more unstable. Indeed, new devices that do not exist today are impossible to predict.

<br>
<img src="img/S2-10.jpg" alt="Disctinction between ICT end-user devices and E&M devices" style="border: 1px solid  gray;">


## Making a digital world

For most people, digital infrastructure is rarely visible. The only immediate contact is through user equipment, yet the infrastructure to enable data processing forms a giant network that crosses oceans and continents until it reaches homes by cable or radio waves. It is this immense infrastructure that allows us to communicate at almost the speed of light. However, it has to adapt to local, territorial and even terrestrial constraints which make its deployment complex and dependent on many parameters.

> Accustomed as we are to the “information revolution,” the accelerating pace of the 24/7 lifestyle, and the multi-connectivity provided by the world wide web, we rarely step back and ask what changes have been occurring at a slower pace, in the background. For the development of cyberinfrastructure, the long now is about 200 years. This is when two sets of changes began to occur in the organization of knowledge and the academies that have accompanied – slowly – the rise of an information infrastructure to support them. On the one hand is the exponential increase in information gathering activities by the state (statistics) and on the other is the emergence of knowledge workers (the encyclopedists) and the accompanying development of technologies and organizational practices to sort, sift, and store information. (Geoffrey C. Bowker, Toward Information Infrastructure Studies: Ways of Knowing in a Networked Environment, p. 103)


## Resources

### Syllabus
* Ingrid Burrington, [Networks of New York – An Illustrated Field Guide to Urban Internet Infrastructure](https://www.mhpbooks.com/books/networks-of-new-york/), <i>Melville House</i>, 2016
* Gauthier Roussilhe, [Territorialising digital systems, the example of data centers](http://gauthierroussilhe.com/post/territoires-centres-de-donnees_en.html), 2021


### To go further
* Geoffrey C. Bowker et al., [Toward Information Infrastructure Studies: Ways of Knowing in a Networked Environment](https://link.springer.com/chapter/10.1007/978-1-4020-9789-8_5), 2010
* Steven Li, [How Does The Internet Work?](https://user3141592.medium.com/how-does-the-internet-work-edc2e22e7eb8), 2017
* Commotion Wireless, [The Commotion Wireless Project - Commotion Construction Kit](https://www.commotionwireless.net/docs/cck/)
* Arzhel Younsi, [Building DReaMeRS: How and why we opened a datacenter in France](https://techblog.wikimedia.org/2022/07/15/building-dreamers-how-and-why-we-opened-a-datacenter-in-france/), <i>Wikimedia Tech blog</i>, 2022
* META, [Meta Data Centers](https://datacenters.fb.com/)
* Barry M. Leiner et al., [A brief history of Internet](http://www.sigcomm.org/node/2811), <i>ACM SIGCOMM Computer Communication Review</i> 39, n.5, 2009

### Video materials
* Wendover Productions, [How Cell Service Actually Works](https://www.youtube.com/watch?v=0faCad2kKeg&list=LL&index=3)
* AT&T Archives, [Similiarities of Wave Behavior](https://www.youtube.com/watch?v=DovunOxlY1k)
* Royal Canadian Air Force, [Antenna Fundamentals: Propagation](https://www.youtube.com/watch?v=7bDyA5t1ldU)