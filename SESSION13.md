# Collective project [2/3]


#### Activity: Continuing collective project (2h)
	After being given instructions, each group works on its assignment




## Session goal

Make an assessment of direct and indirect environmental effects of the digital technology assigned to your groupe by the teacher.

## Role

You are civil servants being asked to deliver a recommendation for the city council and the mayor. Your analysis will help the city refuse, transform or accept the deployment of a given technology. 

During session 7 you drew up an initial analysis of the environmental and climate risks and transition policies in your assigned city. **Now you have to carry out an analysis of the direct and indirect environmental effects of a given digital technology in the same city.**

## Instructions for the collective project

* Stay in the same group as session 7
* Each group keep the same city
* Assess known direct environmental effects of the assigned digital technology (orders of magnitude are sufficient)
* Assess indirect environmental effects of the assigned digital technology using a consequence tree seen in session 9
* Assess the potential evolution of the digital technology (today (2020) to 2050)
* Write a summary of your findings

### Information / data to look for

* Scientific literature (using right keywords in search engine such as [Semantic Scholar](https://www.semanticscholar.org/) or equivalent)
* External analysis from consultancy groups, NGOs, etc.
* Industry reports


### Tips

* The teacher will give to each group a relevant research paper as a starter to constitute a quick literature review
* You can use the consequence tree model seen during session 9 for indrect effects

## Deliverable

Your assessment should take the form of a note (4 pages maximum). Besides text, you can add different media within your summary (interactive maps, graphs, pictures, etc.).






