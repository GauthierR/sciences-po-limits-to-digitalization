# Session 1 - Crossing perspectives


#### Activity: Roundtable (10min)
	What are you interested by this topic? What are your exceptations?


## Why looking at the environmental crisis and digitalization simultaneously?

The ongoing environmental crisis and digitalization are two global phenomena that are deeply modifying human activities. We know today that the environmental crisis is the result of industrial transformations led in England at the beginning of the 19th century and that will later spread in Western Europe, Northern America and East Asia at the end of the 20th century. So it's not Humanity that is responsible but a specific set of societies and efforts to adapt should be distributed accordingly. Furthermore, consequences of the environmental crisis will not be distributed fairly: countries that relied the most on fossil energy for their development will be the ones that would be better suited to adapt and be less impacted. Faced with this situation, we need to be very careful where we want to steer future developments of human activities so they can be sustainable and fairly distributed.

Digitalization is viewed as the next step for economic and technological developments, that could also help move towards ecological transition. Under the hood of digitalization can be found a mix of buzzwords and technological jargon: Artificial Intelligence (AI), Machine Learning (ML), blockchains, autonomous vehicles, smart cities, metaverse, etc, that makes it hard to sort out the significant from the irrelevant. Electronic communications and computing largely change the way we operate industry, research, businesses, public services and even our ways of life. Digitalization accelerated certain activities, transformed all sectors, destroyed others. But does digitalization accelerates activites that provoked / are amplifying the ongoing crisis or is it one of the keys to unlock more sustainable, fair and resilient societies, or both?

<img src="img/S1-01-V2.jpg" alt="Diagram showing a tension between digitalisation of human activities and being a cause for environmental crisis" style="border: 1px solid  gray;">


This fundamental tension between the environmental crisis and digitalization is at the heart of this course. While the scientific community is starting to better understand the environmental footprint of digitalization, it is highly unclear if digitalization, as a whole, has a positive impact and in which conditions it can be fostered. This tension can be somehow visible in the lastest [IPCC's WGIII report](https://report.ipcc.ch/ar6wg3/) :
> At present, the understanding of both the direct and indirect impacts of digitalisation on energy use, carbon emissions and potential mitigation is limited (medium confidence). (Technical Summary, p.132)

This statement is elaborated in the summary for policymakers:

>Digital technologies can contribute to mitigation of climate change and the achievement of several SDGs (high confidence). For example, sensors, Internet of Things, robotics, and artificial intelligence can improve energy management in all sectors, increase energy efficiency, and promote the adoption of many low-emission technologies, including decentralised renewable energy, while creating economic opportunities (high confidence). However, some of these climate change mitigation gains can be reduced or counterbalanced by growth in demand for goods and services due to the use of digital devices (high confidence). Digitalisation can involve trade-offs across several SDGs, e.g., increasing electronic waste, negative impacts on labour markets, and exacerbating the existing digital divide. Digital technology supports decarbonisation only if **appropriately governed** (high confidence). (Summary for Policymakers, p.13)


The "appropriately governed" is a mysterious expression that doesn't specify what is appropriate. A closer look at the technical summary gives better clues :
> System-wide effects endanger energy and GHG emission savings. Rising demand can diminish energy savings, and also produce run-away effects associated with additional consumption and GHG emissions if left unregulated. Savings are varied in smart and shared mobility systems, as ride hailing increases GHG emissions due to deadheading, whereas shared pooled mobility and shared cycling reduce GHG emissions, as occupancy levels and/or weight per person kilometre transported improve. Systemic effects have wider boundaries of analysis and are more difficult to quantify and investigate but are nonetheless very relevant. Systemic effects tend to have negative impacts, but policies and adequate infrastructures and choice architectures can help manage and contain these. (Technical Summary, p.133)

In other words, digitalization can be an enabler for ecological transition if governments constraint overall consumption and induced demand created by digitalization. Furthermore, it implies a powerful state that could go against its digital industry while funding heavily less profitable sectors (infrastructures, etc.). One could wonder if the rise of tech giants and digital companies does not strongly inhibit the emergence of such a state. So far, IPCC's conclusions could be considered as unrealistic, yet part of the tech sector has been build on heavy state funding and protected markets so their entanglement is more concrete than the sector likes to admit. From an environmental crisis perspective, we are left with no clear idea of what to expect from digitalization.


#### Activity: Open debate (15min)
	Do you envision futures that are both digitalized, fair and sustainable?
	What are the alternatives? How to pick which futures are sustainable? 




## Visions of the futures: an ecological and digitalized transition?



Scholars in STS (Science & Technology Studies) talk of sociotechnical imaginaries to describe visions of desirable futures held by advances in science and technology. In the case of digital technologies, Mager & Katzenbach reminds us that these imaginaries are:
* multiple – <i> [...] tracking the trajectories of multiple imaginaries and their relation to one another</i> 
* contested – <i>[...] involves the investigation of more or less explicit contestations and struggles over dominance</i>
* commodified – <i>[...] is often not motivated and propagated by state actors and their interests, but by commercial actors’ assumptions about technology that directly shape the design of their products</i>

> the definition of sociotechnical imaginaries (SIs) encompasses “collectively held, institutionally stabilized, and publicly performed visions of desirable futures, animated by shared understandings of forms of social life and social order attainable through, and supportive of, advances in science and technology” (Jasanoff, 2015 in Mager & Katzenbach, 2021)

We are constantly exposed to new claims that technology <i>X</i> will revolutionized the sector <i>Y</i> in <i>N</i> years. This is not surprising as digital technologies can be a very capital-intensive sector, it means that companies need to keep attracting new investors, growing their customer pool and retain them. To maintain this cycle, the sector produces new claims, reports, pictures, videos, sponsored press coverage at a fast pace. Potentially, building these imaginaires enables to control what we expect from the future, and to make a specific solution inevitable.  

> [...] corporate future imaginaries travel, translate into and gain ground in local contexts, social practices, and even people’s mind. Annette Markham pinpoints this pervasive influence with the notion of “inevitability.” This indicates that technology companies not only take over the imaginative power of shaping future society from state actors, but partly also their ability to govern these very futures with their rhetoric, technologies, and business models. (Mager & Katzenbach, 2021)

<br>
![Remote Robotic Surgean](img/S1-03.jpg)
<br>
<i>A remote 5G robotic surgeon - Credits: AKQA</i><br>

Corporate imaginaries are not meant to be true, they are meant to secure investment and cash flow, and maintain visions of the future where these investments are still relevant, in order to keep investors' trust. This is completely understandable for a company. However, they also greatly reduce the "imaginative power of shaping society", which was previously held by the state, and prevents alternative imaginaries to foster. This only left us with the hope that corporations' imaginaries and development plans are truly aimed to be sustainable, because that could be the only imaginaries left.

> [...] these influential corporations steer the making and governing of digital technology both with their products and with their prophecies. (Mager & Katzenbach, 2021)

<br>
![CES Las Vegas 2022](img/S1-04.jpg)<br>
<i>A metaverse ad at the CES 2022 in Las Vegas - Credits: Unknown</i><br>

Most of these imaginaries are presented under optimal conditions (good weather, good traffic, etc.) and need controled environments to work seamlessly. Yet, the most interesting part of every technology is when it is used in real life conditions, sometimes leading to cocktail effects. In the picture below, you can see delivery robots from a food delivery service blocked by free-floating scooters in an U.S. campus.

<br>
![Blocked delivery robots](img/S1-06.jpg)<br>
<i>Food delivery robots blocked by free-floating scooters - Credits: Sean Hecht</i><br><br>

<iframe width="100%" height="450" src="https://www.youtube-nocookie.com/embed/nwPtcqcqz00" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe><br>

Tech companies don't emphasize primarly on sustainable imaginaries. It is generally assumed that digitalizing is a sustainable process by nature. Therefore, increasing digitalization would only make our world make sustainable. However, recent years showed an increasing concern about this framing, some even estimating that digitalization makes things worse. Before diving into the environmental footprint of digitalization, it seems necessary to understand the assumption that digitalization would be sustainable.


## The environmental history of digitalization

> Technologies become infrastructure only after they are perfected to the point of being routine. We notice them only when they fail. (Nathan Ensmenger, p.14)

Questioning the future of digitalization invites us to reflect on its environmental history. The digital sector and its development have been largely accompanied by discourses of dematerialization or abstraction from the "material world." The historian Nathan Ensmenger summarizes in part the genealogy of ideas that, from Nicholas Negroponte to Diane Coyle, weave the image of a world in which "material resources would be largely useless” , where "the material (atoms) would be replaced by the virtual (bits)” , or present the image of a "weightless" economy.” A founding ideological act of the sector, the declaration of independence of Cyberspace proclaimed in 1996 in Davos, is indicative of these ideas.

> Your legal concepts of property, expression, identity, movement, and context do not apply to us. **They are all based on matter, and there is no matter here** [...] (A Declaration of the Independence of Cyberspace)


Digitized systems and services have their own materiality made of mines, foundries, factories, cargo ships, cables, data centers, antennas, landfills, processing centers, ... Adding this material layer can even lead paradoxically to a second materialization if physical and digital services are superimposed for the same service. Several recent studies question the prospect of dematerialization linked to digitalization. In a 2016 research article examining material consumption trends over the past few decades, Magee and Devezas conclude, "...the extension of [dematerialization] theory and empirical examination indicate that dematerialization does not occur, even for cases of information technology with rapid technical progress. Thus, a completely passive policy that relies on unfettered technological change is not supported by our results.” Despite this, the theory of the immateriality of digital technologies is still widely present in the collective imagination, for example with a term like cloud (hosting on a remote "virtualized" server) and by the perpetuation of the camouflage and discretion of infrastructures.

> If we focus solely on the consumption side of computing and information technology, this model of a radical, discontinuous break with our industrial history does seem plausible. For the most part, we experience only the positive benefits of information technology, in large part because the labor and geography associated with the construction, maintenance, and dismantling our digital devices has been rendered largely invisible. (Nathan Ensmenger, p.10)

<br>
![Camouflage](img/S1-09.jpg)<br>
<i>Antennas and palm trees - Credits: Annette Lemay Burke</i><br>

The idea of dematerialization calls also for an "anti-geographic" thesis (Marquet, 2019), that is, the idea of an “abolition of space and the dissolution of cities by digital technologies.” This thesis would have been supported by a number of influential people such as Marshall McLuhan, Alvin Toffler, Paul Virilio, Nicolas Negroponte, Bill Gates or Manuel Castells . In line with this idea, Frances Cairncross proposes "the death of distance" to undermine "the importance of geographic concentrations in cities" in a world where information and communication technologies (ICTs) are revolutionizing social and professional relationships. This perspective of a dissolution of space could be described as a movement of deterritorialization together with that of dematerialization: without materiality to describe and to place in the thickness of space, there is possibly no more territory to describe. The "anti-geographic" or deterritorialization thesis could lead us to think of the digital infrastructure and its services as a sheet that is laid down and covers the entire globe in a homogeneous manner, indifferent to any local roughness or context.

> The Information infrastructure of the twenty-first century is built around the bones of the nineteenth-century transportation and communication networks. These were in turn constructed along riverbeds and mountain passes. Geography shapes technology, and vice versa. (Nathan Ensmenger, p.19)

Understanding the digital sector and its systems is a complex exercise, given that they exist simultaneously in two states: concentrated and diluted. On the one hand, there is a significant concentration of capital, value capture, means of production, and infrastructure, and on the other hand, digitalization is diffusely transforming all sectors of activity: industry, public policy, logistics, finance, entertainment, etc. Thus, the material description of the digital sector poses two problems a priori: a conceptual problem, how to describe the material realities of a sector that has forged an immaterial imaginary; and a problem of perimeter, in a sector that is not equally distributed but that touches most of the activities on Earth, where does the description end? Similarly, if we admit that the ideas of dematerialization and deterritorialization have developed together, how can we stitch material and territorial realities together? This requires us to define what the materiality and territoriality of the digital sector are and to trace them methodically before we can even integrate the issues of the environmental crisis.

> Automobiles pollute, nuclear energy produces toxic waste, industrial agriculture is giving us all cancer—but computers keep getting faster, smaller, better. By making the physical world increasingly irrelevant, information technologies allow us to avoid confronting the consequences of our actions on the environment. (Nathan Ensmenger, p.26)


### The case of the Silicon Valley

> Silicon Valley was fertile ground for a major environmental and occupational health controversy in the late 1970s and 1980s. It had a very large industry that had contaminated many aquifers and severely affected the health of thousands of workers. But it is very likely that the full extent of pollution and workplace hazards would never have been known and addressed had it not been for the drive, courage, and tenacity of a group of young women: Amanda Hawes, Patricia Lamborn, and Robin Baker of SCCOSH; Marta Rojas, Cathi Hee, and Cathy Bauerle of Signetics; Susan Yoachum of the San Jose Mercury News; and Lorraine Ross and her San Jose neighbors. [...] Without them, the Valley might have remained the “clean” and “safe” place it used to be in the 1960s and 1970s. (Christophe Lécuyer, p.326)

The Silicon Valley shows an interesting paradox on the matter. It is one of the birthplace of innovation and modern technologies, especially semiconductors, it is also one of the regions with the most Superfund sites, i.e. the most polluted locations, in the nation. The myth of the immateriality was conceived mostly by people that gravitated around this area, so how come they did not see these polluting activites that were providing them chips and computers? 

>The growing number of polluted sites came as a major surprise to engineers and managers working in the semiconductor industry. “I was shocked,” an AMD engineer later recalled. “Everybody was. We had **all considered the chip industry to be super-clean, all being proved by our smokeless chimneys, clean green lawns, and nice-looking buildings**. The leaking solvents problem surprised everyone in the Valley.” (Christophe Lécuyer, p.322)

The environmental movement in the Silicon Valley is quite unsual because it was spearheaded by labor activits, according to Christophe Lécuyer. They attacked microelectronics companies on their poor safety records to unionize the industry. While their unionizing effort failed, they led to the discovery of large-scale water pollution due to leaking underground tanks. Companies were forced to improve safety but scandals kept coming (in Los Paseos, miscarriages and birth defects were three times higher than in the whole California). This situation deeply modified the credibility of microelectronics firms and led to the creation of the EPA's superfund program. Today, a large part of the semiconductor manufacturing is outsourced in Asia, especially in Japan, South Korea, Taiwan and China so these environmental issues have almost disapparead in California. (See interactive map of U.S. superfund sites [here](https://www.nationalgeographic.com/superfund/) and [here](https://epa.maps.arcgis.com/apps/webappviewer/index.html?id=33cebcdfdd1b4c3a8b51d416956c41f1))

<br>
![Superfund sites](img/S1-11.png)
<i>U.S. map of superfund sites - Credits: National Geographic</i><br>

The environmental movement in the Silicon Valley was mainly led by the left wing of the labor movement (confrontational, antimanagement, anticapitalist, and prounion). However, corporations told a very different story than the grassroot labor movement and shaped the imaginaries around the Silicon Valley and its industry. We can only assume that thinkers from the Valley couldn't (or decided not to) see the environemental damage unfolding down south and rather follow the storyline displayed by the semiconductor manufacturing industry.

> The history of antitoxics organizing in Silicon Valley is full of paradoxes. It started with a campaign to unionize the microelectronics industry and ended up initiating a massive, federally mandated environmental remediation program. Labor activists turned into environmentalists, and the organization that drove the movement splintered as it obtained its greatest success. Social activism is full of uncertainty indeed. (Christophe Lécuyer, p.327)

## Futures without history

This brief exploration of digital technologies imaginaries show that first we need to reconstruct the environmental history of the digital sector as it has been shaped by a very selective bundle of ideas and theories. This bundle has tended to ignore the environmental consequences that have accompanied the emergence of modern electronics and computing, while they were taking place in front of the eyes of those who maintained the ideas of immateriality of digitalization. The power of the imaginaries we construct is such that it can allow us to ignore very real facts, from the environmental pollution of Silicon Valley to climate change. And yet, we are still building futures of digitalization that rely on these imaginaries that have denied material and environmental realities from the start. The process of reconstructing our digital imaginaries in the era of the environmental crisis will have to pass through a reconstruction of the materiality and the terriroriality of digital infrastructures. Only then will we be able to plan better for this uncertain century.

<br>
![Twitter](img/S1-10.png)<br>
<i>Credits: Ben Reinhardt</i><br>





## Resources

### Syllabus
* Astrid Mager, Christian Katzenbach, [Future imaginaries in the making and governing of digital technology: Multiple, contested, commodified](https://journals.sagepub.com/doi/abs/10.1177/1461444820929321), <i>New Media & Society</i> 23, n.2, 2021
* Nathan Ensmenger, [The Environmental History of Computing](https://muse.jhu.edu/article/712112/pdf), <i>Technology and Culture</i> 59, n.4, 2018
* Christophe Lécuyer, [From Clean Rooms to Dirty Water: Labor, Semiconductor Firms, and the Struggle over Pollution and Workplace Hazards in Silicon Valley](https://www.utexaspressjournals.org/doi/abs/10.7560/IC52302), Information & Culture: A Journal of History 52, n.3, 2017



### To go further
* Sheila Jasanoff, [A New Climate for Society](https://journals.sagepub.com/doi/pdf/10.1177/0263276409361497), <i>Theory, Culture & Society</i> 27, n.2-3, 2010
* Christopher L. Magee et Tessaleno C. Devezas, [A simple extension of dematerialization theory: Incorporation of technical progress and the rebound effect](https://www.sciencedirect.com/science/article/pii/S0040162516308022), <i>Technological Forecasting and Social Change</i> 117, 2017, p. 196.
* Clément Marquet, [Binaire béton : Quand les infrastructures numériques aménagent la ville](https://pastel.archives-ouvertes.fr/tel-02486197), Thèse de sociologie, Université Paris-Saclay, 2019, p. 21.